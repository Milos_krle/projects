﻿
namespace AltosGo
{
    partial class AltosGo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AltosGo));
            this.StopericaRazgovora = new System.Windows.Forms.Timer(this.components);
            this.LbKomande = new System.Windows.Forms.ListBox();
            this.pBAnimacija = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pBAnimacija)).BeginInit();
            this.SuspendLayout();
            // 
            // StopericaRazgovora
            // 
            this.StopericaRazgovora.Enabled = true;
            this.StopericaRazgovora.Interval = 1000;
            this.StopericaRazgovora.Tick += new System.EventHandler(this.StopericaRazgovora_Tick);
            // 
            // LbKomande
            // 
            this.LbKomande.BackColor = System.Drawing.SystemColors.InfoText;
            this.LbKomande.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LbKomande.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.LbKomande.FormattingEnabled = true;
            this.LbKomande.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LbKomande.Location = new System.Drawing.Point(1, 1);
            this.LbKomande.MultiColumn = true;
            this.LbKomande.Name = "LbKomande";
            this.LbKomande.Size = new System.Drawing.Size(177, 559);
            this.LbKomande.TabIndex = 0;
            this.LbKomande.Visible = false;
            // 
            // pBAnimacija
            // 
            this.pBAnimacija.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pBAnimacija.Location = new System.Drawing.Point(575, 212);
            this.pBAnimacija.Name = "pBAnimacija";
            this.pBAnimacija.Size = new System.Drawing.Size(197, 86);
            this.pBAnimacija.TabIndex = 1;
            this.pBAnimacija.TabStop = false;
            this.pBAnimacija.Visible = false;
            // 
            // AltosGo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pBAnimacija);
            this.Controls.Add(this.LbKomande);
            this.MaximumSize = new System.Drawing.Size(800, 600);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "AltosGo";
            this.Text = "AltosGo";
            this.Load += new System.EventHandler(this.AltosGo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pBAnimacija)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer StopericaRazgovora;
        private System.Windows.Forms.ListBox LbKomande;
        private System.Windows.Forms.PictureBox pBAnimacija;
    }
}

