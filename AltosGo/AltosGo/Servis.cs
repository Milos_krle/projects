﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.IO;

namespace AltosGo
{
    public class Servis
    {
        public SpeechRecognitionEngine Prepoznaj { get; set; }
        public SpeechRecognitionEngine Slusaj { get; set; }

        public SpeechSynthesizer Altos { get; set; }

        public Random VremeRazgovora = new Random();
        public int vremeR = 0;
        public DateTime vreme = new DateTime();
        public Servis()
        {
            Prepoznaj = new SpeechRecognitionEngine();
            Slusaj = new SpeechRecognitionEngine();
            Altos = new SpeechSynthesizer();

            Prepoznaj.SetInputToDefaultAudioDevice();
            Prepoznaj.LoadGrammarAsync(new Grammar (new GrammarBuilder (new Choices (File.ReadAllLines(@"Komande.txt")))));
            Prepoznaj.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(Default_SpeechRecognized);
            Prepoznaj.SpeechDetected += new EventHandler<SpeechDetectedEventArgs>(Prepoznaj_SpeechRecognized);
            Prepoznaj.RecognizeAsync(RecognizeMode.Multiple);


            Slusaj.SetInputToDefaultAudioDevice();
            Slusaj.LoadGrammarAsync(new Grammar(new GrammarBuilder(new Choices(File.ReadAllLines(@"Komande.txt")))));
            Slusaj.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(Slusaj_SpeechRecognized);
        
        
        }

        private void Default_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            int ranNum;
            string govor = e.Result.Text;

            switch(govor)
            {
                case "Hello":
                    {
                        Altos.SpeakAsync("Hello, I am here");
                        break;
                    }
                case "How are you":
                    {
                        Altos.SpeakAsync("I am fine, how are you");
                        break;
                    }
                case "What time is it":
                    {
                        Altos.SpeakAsync("Time is " + DateTime.Now.ToString("hh:mm:ss"));
                        break;
                    }
                case "Stop talking":
                    {
                        Altos.SpeakAsyncCancelAll();
                        ranNum = VremeRazgovora.Next(1,2);
                        if (ranNum == 1)
                        {
                            Altos.SpeakAsync("Yes sir");
                        }
                       
                         if (ranNum == 2)
                          {
                            Altos.SpeakAsync("I am sorry i will be quiet");
                          }
                        

                        break;
                    }
                case "Stop listening":
                    {

                        Altos.SpeakAsync("If you need me just say");
                        Prepoznaj.RecognizeAsyncCancel();
                        Slusaj.RecognizeAsync(RecognizeMode.Multiple);

                        break;
                    }
                case "Show commands":
                    {
                        string[] commands =  File.ReadAllLines(@"Komande.txt");
                        AltosGo a = new AltosGo();
                      //  a.SetListBox(commands);
                        break;
                    }
                case "Hide commands":
                    {
                        AltosGo a = new AltosGo();
                      //  a.SetLBDis(); 
                        break;
                    }
                //case "":
                //    {
                //        break;
                //    }
          

                default:
                    {
                        Altos.SpeakAsync("I don t understand");
                        break;
                    }
            }
     
        }
         
        private void Prepoznaj_SpeechRecognized(object sender, SpeechDetectedEventArgs e)
        {
            vremeR = 0;

        }
        private void Slusaj_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            string govor = e.Result.Text;
            switch (govor)
            {
                case "Wake up":
                    {
                        Slusaj.RecognizeAsyncCancel();
                        Altos.SpeakAsync("Yes, i am hire");
                        Prepoznaj.RecognizeAsync(RecognizeMode.Multiple);
                        break;
                    }
                case "How are you":
                    {
                        Altos.SpeakAsync("I am fine, how are you");
                        break;
                    }
                case "What time is it":
                    {
                        Altos.SpeakAsync("Time is " + DateTime.Now.ToString("hh:mm:ss"));
                        break;
                    }
                case "Altos":
                    {
                        Slusaj.RecognizeAsyncCancel();
                        Altos.SpeakAsync("Yes, i am hire");
                        Prepoznaj.RecognizeAsync(RecognizeMode.Multiple);
                        break;
                    }

                default:
                    {
                        Altos.SpeakAsync("I don t understand");
                        break;
                    }
            }

        }

      



        public void DodajKomandu(string komanda)
        {
            //uzme iz drugu formu i uneses komandu 
        }

        
    }
}