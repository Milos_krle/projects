﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace OOPLab1
{
	interface IUlaz_Izlaz
	{
		 void Ucitaj_iz_Fajl(StreamReader sr);
		 void Upisi_u_Fajl(StreamWriter sw);
	}
}
