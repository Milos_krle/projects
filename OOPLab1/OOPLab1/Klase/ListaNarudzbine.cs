﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPLab1
{
	public class ListaNarudzbine 
	{
		// Sigleton
		private List<Narudzbina> nizNarudzbina;
		private List<Narudzbina> filtriraniNizNarudzbina;
		private static ListaNarudzbine listNarudzbinee = null;
		public List<Narudzbina> NizNarudzbina { get => nizNarudzbina; set => nizNarudzbina = value; }
		public List<Narudzbina> FiltriraniNizNarudzbina { get => filtriraniNizNarudzbina; set => filtriraniNizNarudzbina = value; }
		public static ListaNarudzbine ListNarudzbinee1 { get => listNarudzbinee; set => listNarudzbinee = value; }

		protected ListaNarudzbine()
		{
			NizNarudzbina = new List<Narudzbina>();
			FiltriraniNizNarudzbina = new List<Narudzbina>();
		}

		public static ListaNarudzbine ListNarudzbinee
		{
			get
			{
				if (listNarudzbinee == null)
				{
					listNarudzbinee = new ListaNarudzbine();
				}
				return listNarudzbinee;
			}
		}

		

		public void Ucitaj_iz_Fajl(string file)
		{
			if (nizNarudzbina.Count != 0)
			{
				nizNarudzbina.Clear();
			}
			try
			{
				using (StreamReader sr = new StreamReader(file))
				{
					int n = Int32.Parse(sr.ReadLine());
					for (int i=0; i<=n; i++)
					{
						Narudzbina narudz = new Narudzbina();
						narudz.Ucitaj_iz_Fajl(sr);
						nizNarudzbina.Add(narudz);
					}
				}
				ObrisiDuplikate();
			}
			catch(IOException e)
			{
				Console.WriteLine("Greska prilikom citanja iz fajla " + e);
			}

		}

		public void Upisi_u_FajlListu(string filename)
		{
			try
			{
				using (StreamWriter sw = new StreamWriter(filename))
				{
					foreach(var i in nizNarudzbina)
					{
						i.Upisi_u_Fajl(sw);
					}
				}
			}
			catch(IOException e)
			{
				Console.WriteLine("Izuzetak pri upisu u fajl " + e);
			}
		}

		public void Upisi_u_Fajl_Samo_Jedan_Red(string filen)
		{
			try
			{
				using (StreamWriter sw = new StreamWriter(filen))
				{
					Narudzbina nar = nizNarudzbina.Last();

					nar.Upisi_u_Fajl(sw);
				}
			}
			catch (IOException e)
			{
				Console.WriteLine("Greska prilikom upisa u fajl" + e);
			}
		}

		public void Ucitaj_iz_Fajl_Samo_Jedan_Red(string filen)
		{
			try
			{
				using (StreamReader sr = new StreamReader(filen))
				{
					Narudzbina nar = new Narudzbina();
					nar.Ucitaj_iz_Fajl(sr);
					nizNarudzbina.Add(nar);
				}
			}
			catch (IOException e)
			{
				Console.WriteLine("Greska prilikom citanja iz fajl" + e);
			}
		}
		public void ObrisiDuplikate()
		{
			if (ListaNarudzbine.listNarudzbinee.NizNarudzbina.Count > 1)
			{
				for (int i = 0; i < ListaNarudzbine.listNarudzbinee.NizNarudzbina.Count; i++)
				{
					for (int j = 0; j < ListaNarudzbine.listNarudzbinee.NizNarudzbina.Count; j++)
					{
						if ((ListaNarudzbine.listNarudzbinee.NizNarudzbina[i].JedBrNarudzbine1 == ListaNarudzbine.listNarudzbinee.NizNarudzbina[j].JedBrNarudzbine1) && (i != j))
						{
							ListaNarudzbine.listNarudzbinee.NizNarudzbina.RemoveAt(j);
						}
					}
				}
			}
		}
		public void DodajNarudzbinu(Narudzbina nar)
		{
			if (nar != null)
			{
				this.nizNarudzbina.Add(nar);
			}
		}
		public void ObrisiNarudzbinu(Narudzbina nar)
		{
			if (nar != null)
			{
				this.nizNarudzbina.Remove(nar);
			}
		}
		public Narudzbina PronadjiNarudzbinu(string idnar)
		{
			foreach(Narudzbina i in nizNarudzbina)
			{
				if (i.JedBrNarudzbine1 == idnar)
				{
					return i;
				}
			}
			return null;
		}

	}
}
