﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPLab1
{
	
	public class Proizvod : IUlaz_Izlaz
	{

		private string imeProizvoda;
		private decimal cenaProizvoda;
		private uint kolicinaProizvoda;
		
		public Proizvod()
		{
			this.cenaProizvoda = 0;
			this.imeProizvoda = "";
			this.kolicinaProizvoda = 0;
		}
		public Proizvod(string ImeP, decimal cenaPro, uint koicinaPro)
		{
			this.cenaProizvoda = 0;
			this.imeProizvoda = "";
			this.kolicinaProizvoda = koicinaPro ;
		}

		public string ImeProizvoda { get => imeProizvoda; set => imeProizvoda = value; }
		public decimal CenaProizvoda { get => cenaProizvoda; set => cenaProizvoda = value; }
		public uint KolicinaProizvoda { get => kolicinaProizvoda; set => kolicinaProizvoda = value; }

		public void Ucitaj_iz_Fajl(StreamReader sr)
		{
			this.imeProizvoda = sr.ReadLine();
			this.cenaProizvoda = Decimal.Parse(sr.ReadLine());
			this.kolicinaProizvoda = UInt32.Parse(sr.ReadLine());
		}

		public void Upisi_u_Fajl(StreamWriter sw)
		{
			sw.WriteLine(this.imeProizvoda);
			sw.WriteLine(this.cenaProizvoda.ToString());
			sw.WriteLine(this.kolicinaProizvoda.ToString());
		}
		
	}
}
