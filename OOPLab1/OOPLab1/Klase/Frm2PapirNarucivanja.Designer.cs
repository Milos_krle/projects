﻿namespace OOPLab1
{
	partial class Frm2PapirNarucivanja
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
			this.gB1Podaci = new System.Windows.Forms.GroupBox();
			this.lblFirma = new System.Windows.Forms.Label();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.lb7Potrosac = new System.Windows.Forms.Label();
			this.tb1Potrosac = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.lb6CenaPrevoza = new System.Windows.Forms.Label();
			this.lb5Prevoznik = new System.Windows.Forms.Label();
			this.lb4DatumIsporuke = new System.Windows.Forms.Label();
			this.lb3DatumNajkIsporuke = new System.Windows.Forms.Label();
			this.dT2NajkDanIsporuke = new System.Windows.Forms.DateTimePicker();
			this.dT4DatumIsporuke = new System.Windows.Forms.DateTimePicker();
			this.tb5Prevoznik = new System.Windows.Forms.TextBox();
			this.tb6CenaPrevoza = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.lb1IDNaruzbenice = new System.Windows.Forms.Label();
			this.lb2DatumPorudzbine = new System.Windows.Forms.Label();
			this.tbIDNarudzbin1 = new System.Windows.Forms.TextBox();
			this.dT1DatumPorudzbine = new System.Windows.Forms.DateTimePicker();
			this.cb1Stanje = new System.Windows.Forms.ComboBox();
			this.lb8Stanje = new System.Windows.Forms.Label();
			this.GrStavkeNarudzbine = new System.Windows.Forms.GroupBox();
			this.btm2Izbaci = new System.Windows.Forms.Button();
			this.btm1Ubaci = new System.Windows.Forms.Button();
			this.tb3Kolicina = new System.Windows.Forms.TextBox();
			this.tb2Cena = new System.Windows.Forms.TextBox();
			this.tb1UnesiImeProizvoda = new System.Windows.Forms.TextBox();
			this.dataGridViewProizvoda = new System.Windows.Forms.DataGridView();
			this.btmPOTVRDI = new System.Windows.Forms.Button();
			this.btmOtkazi = new System.Windows.Forms.Button();
			this.gB1Podaci.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.GrStavkeNarudzbine.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProizvoda)).BeginInit();
			this.SuspendLayout();
			// 
			// gB1Podaci
			// 
			this.gB1Podaci.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.gB1Podaci.Controls.Add(this.lblFirma);
			this.gB1Podaci.Controls.Add(this.tableLayoutPanel3);
			this.gB1Podaci.Controls.Add(this.tableLayoutPanel2);
			this.gB1Podaci.Controls.Add(this.tableLayoutPanel1);
			this.gB1Podaci.Location = new System.Drawing.Point(12, 12);
			this.gB1Podaci.Name = "gB1Podaci";
			this.gB1Podaci.Size = new System.Drawing.Size(736, 271);
			this.gB1Podaci.TabIndex = 0;
			this.gB1Podaci.TabStop = false;
			this.gB1Podaci.Text = "Podaci";
			// 
			// lblFirma
			// 
			this.lblFirma.AutoSize = true;
			this.lblFirma.Font = new System.Drawing.Font("Bookman Old Style", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblFirma.Location = new System.Drawing.Point(48, 21);
			this.lblFirma.Name = "lblFirma";
			this.lblFirma.Size = new System.Drawing.Size(407, 38);
			this.lblFirma.TabIndex = 4;
			this.lblFirma.Text = "      ACME Corporation \r\n";
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
			this.tableLayoutPanel3.ColumnCount = 1;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
			this.tableLayoutPanel3.Controls.Add(this.lb7Potrosac, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.tb1Potrosac, 0, 1);
			this.tableLayoutPanel3.Location = new System.Drawing.Point(56, 153);
			this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(2);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 2;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(665, 102);
			this.tableLayoutPanel3.TabIndex = 3;
			// 
			// lb7Potrosac
			// 
			this.lb7Potrosac.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lb7Potrosac.BackColor = System.Drawing.Color.Coral;
			this.lb7Potrosac.Location = new System.Drawing.Point(5, 3);
			this.lb7Potrosac.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lb7Potrosac.Name = "lb7Potrosac";
			this.lb7Potrosac.Size = new System.Drawing.Size(655, 46);
			this.lb7Potrosac.TabIndex = 0;
			this.lb7Potrosac.Text = "Potrosac";
			this.lb7Potrosac.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tb1Potrosac
			// 
			this.tb1Potrosac.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tb1Potrosac.Location = new System.Drawing.Point(5, 54);
			this.tb1Potrosac.Margin = new System.Windows.Forms.Padding(2);
			this.tb1Potrosac.MaxLength = 100;
			this.tb1Potrosac.Multiline = true;
			this.tb1Potrosac.Name = "tb1Potrosac";
			this.tb1Potrosac.Size = new System.Drawing.Size(655, 43);
			this.tb1Potrosac.TabIndex = 1;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
			this.tableLayoutPanel2.ColumnCount = 4;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.Controls.Add(this.lb6CenaPrevoza, 3, 0);
			this.tableLayoutPanel2.Controls.Add(this.lb5Prevoznik, 2, 0);
			this.tableLayoutPanel2.Controls.Add(this.lb4DatumIsporuke, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.lb3DatumNajkIsporuke, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.dT2NajkDanIsporuke, 0, 1);
			this.tableLayoutPanel2.Controls.Add(this.dT4DatumIsporuke, 1, 1);
			this.tableLayoutPanel2.Controls.Add(this.tb5Prevoznik, 2, 1);
			this.tableLayoutPanel2.Controls.Add(this.tb6CenaPrevoza, 3, 1);
			this.tableLayoutPanel2.Location = new System.Drawing.Point(55, 82);
			this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 2;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(666, 54);
			this.tableLayoutPanel2.TabIndex = 2;
			// 
			// lb6CenaPrevoza
			// 
			this.lb6CenaPrevoza.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lb6CenaPrevoza.BackColor = System.Drawing.Color.Coral;
			this.lb6CenaPrevoza.Location = new System.Drawing.Point(500, 3);
			this.lb6CenaPrevoza.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lb6CenaPrevoza.Name = "lb6CenaPrevoza";
			this.lb6CenaPrevoza.Size = new System.Drawing.Size(161, 22);
			this.lb6CenaPrevoza.TabIndex = 3;
			this.lb6CenaPrevoza.Text = "Cena prevoza";
			this.lb6CenaPrevoza.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lb5Prevoznik
			// 
			this.lb5Prevoznik.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lb5Prevoznik.BackColor = System.Drawing.Color.Coral;
			this.lb5Prevoznik.Location = new System.Drawing.Point(335, 3);
			this.lb5Prevoznik.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lb5Prevoznik.Name = "lb5Prevoznik";
			this.lb5Prevoznik.Size = new System.Drawing.Size(158, 22);
			this.lb5Prevoznik.TabIndex = 2;
			this.lb5Prevoznik.Text = "Prevoznik";
			this.lb5Prevoznik.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lb4DatumIsporuke
			// 
			this.lb4DatumIsporuke.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lb4DatumIsporuke.BackColor = System.Drawing.Color.Coral;
			this.lb4DatumIsporuke.Location = new System.Drawing.Point(170, 3);
			this.lb4DatumIsporuke.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lb4DatumIsporuke.Name = "lb4DatumIsporuke";
			this.lb4DatumIsporuke.Size = new System.Drawing.Size(158, 22);
			this.lb4DatumIsporuke.TabIndex = 1;
			this.lb4DatumIsporuke.Text = "Datum isporuke";
			this.lb4DatumIsporuke.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lb3DatumNajkIsporuke
			// 
			this.lb3DatumNajkIsporuke.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lb3DatumNajkIsporuke.BackColor = System.Drawing.Color.Coral;
			this.lb3DatumNajkIsporuke.Location = new System.Drawing.Point(5, 3);
			this.lb3DatumNajkIsporuke.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lb3DatumNajkIsporuke.Name = "lb3DatumNajkIsporuke";
			this.lb3DatumNajkIsporuke.Size = new System.Drawing.Size(158, 22);
			this.lb3DatumNajkIsporuke.TabIndex = 0;
			this.lb3DatumNajkIsporuke.Text = "Najkasniji dan isporuke";
			this.lb3DatumNajkIsporuke.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// dT2NajkDanIsporuke
			// 
			this.dT2NajkDanIsporuke.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dT2NajkDanIsporuke.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dT2NajkDanIsporuke.Location = new System.Drawing.Point(5, 30);
			this.dT2NajkDanIsporuke.Margin = new System.Windows.Forms.Padding(2);
			this.dT2NajkDanIsporuke.Name = "dT2NajkDanIsporuke";
			this.dT2NajkDanIsporuke.Size = new System.Drawing.Size(158, 20);
			this.dT2NajkDanIsporuke.TabIndex = 4;
			// 
			// dT4DatumIsporuke
			// 
			this.dT4DatumIsporuke.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dT4DatumIsporuke.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dT4DatumIsporuke.Location = new System.Drawing.Point(170, 30);
			this.dT4DatumIsporuke.Margin = new System.Windows.Forms.Padding(2);
			this.dT4DatumIsporuke.Name = "dT4DatumIsporuke";
			this.dT4DatumIsporuke.Size = new System.Drawing.Size(158, 20);
			this.dT4DatumIsporuke.TabIndex = 5;
			// 
			// tb5Prevoznik
			// 
			this.tb5Prevoznik.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tb5Prevoznik.Location = new System.Drawing.Point(335, 30);
			this.tb5Prevoznik.Margin = new System.Windows.Forms.Padding(2);
			this.tb5Prevoznik.Name = "tb5Prevoznik";
			this.tb5Prevoznik.Size = new System.Drawing.Size(158, 20);
			this.tb5Prevoznik.TabIndex = 6;
			// 
			// tb6CenaPrevoza
			// 
			this.tb6CenaPrevoza.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tb6CenaPrevoza.Location = new System.Drawing.Point(500, 30);
			this.tb6CenaPrevoza.Margin = new System.Windows.Forms.Padding(2);
			this.tb6CenaPrevoza.Name = "tb6CenaPrevoza";
			this.tb6CenaPrevoza.Size = new System.Drawing.Size(161, 20);
			this.tb6CenaPrevoza.TabIndex = 7;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.lb1IDNaruzbenice, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.lb2DatumPorudzbine, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.tbIDNarudzbin1, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.dT1DatumPorudzbine, 1, 1);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(477, 18);
			this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(244, 50);
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// lb1IDNaruzbenice
			// 
			this.lb1IDNaruzbenice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lb1IDNaruzbenice.BackColor = System.Drawing.Color.Coral;
			this.lb1IDNaruzbenice.Location = new System.Drawing.Point(5, 3);
			this.lb1IDNaruzbenice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lb1IDNaruzbenice.Name = "lb1IDNaruzbenice";
			this.lb1IDNaruzbenice.Size = new System.Drawing.Size(113, 20);
			this.lb1IDNaruzbenice.TabIndex = 0;
			this.lb1IDNaruzbenice.Text = "ID Narudzbine";
			this.lb1IDNaruzbenice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lb2DatumPorudzbine
			// 
			this.lb2DatumPorudzbine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lb2DatumPorudzbine.BackColor = System.Drawing.Color.Coral;
			this.lb2DatumPorudzbine.Location = new System.Drawing.Point(125, 3);
			this.lb2DatumPorudzbine.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lb2DatumPorudzbine.Name = "lb2DatumPorudzbine";
			this.lb2DatumPorudzbine.Size = new System.Drawing.Size(114, 20);
			this.lb2DatumPorudzbine.TabIndex = 1;
			this.lb2DatumPorudzbine.Text = "Datum porudzbine";
			this.lb2DatumPorudzbine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tbIDNarudzbin1
			// 
			this.tbIDNarudzbin1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tbIDNarudzbin1.Location = new System.Drawing.Point(5, 28);
			this.tbIDNarudzbin1.Margin = new System.Windows.Forms.Padding(2);
			this.tbIDNarudzbin1.MaxLength = 8;
			this.tbIDNarudzbin1.Multiline = true;
			this.tbIDNarudzbin1.Name = "tbIDNarudzbin1";
			this.tbIDNarudzbin1.Size = new System.Drawing.Size(113, 17);
			this.tbIDNarudzbin1.TabIndex = 2;
			this.tbIDNarudzbin1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.tbIDNarudzbin1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIDNarudzbin1_KeyPress);
			// 
			// dT1DatumPorudzbine
			// 
			this.dT1DatumPorudzbine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dT1DatumPorudzbine.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dT1DatumPorudzbine.Location = new System.Drawing.Point(125, 28);
			this.dT1DatumPorudzbine.Margin = new System.Windows.Forms.Padding(2);
			this.dT1DatumPorudzbine.Name = "dT1DatumPorudzbine";
			this.dT1DatumPorudzbine.Size = new System.Drawing.Size(114, 20);
			this.dT1DatumPorudzbine.TabIndex = 3;
			// 
			// cb1Stanje
			// 
			this.cb1Stanje.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.cb1Stanje.DisplayMember = "Obrada";
			this.cb1Stanje.FormattingEnabled = true;
			this.cb1Stanje.Items.AddRange(new object[] {
            "Ispunjavanje",
            "Obrada",
            "Kompletan"});
			this.cb1Stanje.Location = new System.Drawing.Point(636, 375);
			this.cb1Stanje.Margin = new System.Windows.Forms.Padding(2);
			this.cb1Stanje.Name = "cb1Stanje";
			this.cb1Stanje.Size = new System.Drawing.Size(92, 21);
			this.cb1Stanje.TabIndex = 13;
			this.cb1Stanje.Text = "Stanje";
			// 
			// lb8Stanje
			// 
			this.lb8Stanje.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lb8Stanje.AutoSize = true;
			this.lb8Stanje.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lb8Stanje.Location = new System.Drawing.Point(656, 344);
			this.lb8Stanje.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lb8Stanje.Name = "lb8Stanje";
			this.lb8Stanje.Size = new System.Drawing.Size(48, 17);
			this.lb8Stanje.TabIndex = 12;
			this.lb8Stanje.Text = "Stanje";
			// 
			// GrStavkeNarudzbine
			// 
			this.GrStavkeNarudzbine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GrStavkeNarudzbine.Controls.Add(this.btm2Izbaci);
			this.GrStavkeNarudzbine.Controls.Add(this.btm1Ubaci);
			this.GrStavkeNarudzbine.Controls.Add(this.tb3Kolicina);
			this.GrStavkeNarudzbine.Controls.Add(this.tb2Cena);
			this.GrStavkeNarudzbine.Controls.Add(this.tb1UnesiImeProizvoda);
			this.GrStavkeNarudzbine.Controls.Add(this.dataGridViewProizvoda);
			this.GrStavkeNarudzbine.Location = new System.Drawing.Point(67, 309);
			this.GrStavkeNarudzbine.Margin = new System.Windows.Forms.Padding(2);
			this.GrStavkeNarudzbine.Name = "GrStavkeNarudzbine";
			this.GrStavkeNarudzbine.Padding = new System.Windows.Forms.Padding(2);
			this.GrStavkeNarudzbine.Size = new System.Drawing.Size(538, 187);
			this.GrStavkeNarudzbine.TabIndex = 11;
			this.GrStavkeNarudzbine.TabStop = false;
			this.GrStavkeNarudzbine.Text = "Stavke narudzbine";
			// 
			// btm2Izbaci
			// 
			this.btm2Izbaci.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.btm2Izbaci.Location = new System.Drawing.Point(474, 121);
			this.btm2Izbaci.Margin = new System.Windows.Forms.Padding(2);
			this.btm2Izbaci.Name = "btm2Izbaci";
			this.btm2Izbaci.Size = new System.Drawing.Size(56, 22);
			this.btm2Izbaci.TabIndex = 8;
			this.btm2Izbaci.Text = "Izbaci";
			this.btm2Izbaci.UseVisualStyleBackColor = true;
			this.btm2Izbaci.Click += new System.EventHandler(this.btm2Izbaci_Click);
			// 
			// btm1Ubaci
			// 
			this.btm1Ubaci.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.btm1Ubaci.Location = new System.Drawing.Point(475, 84);
			this.btm1Ubaci.Margin = new System.Windows.Forms.Padding(2);
			this.btm1Ubaci.Name = "btm1Ubaci";
			this.btm1Ubaci.Size = new System.Drawing.Size(56, 22);
			this.btm1Ubaci.TabIndex = 7;
			this.btm1Ubaci.Text = "Ubaci";
			this.btm1Ubaci.UseVisualStyleBackColor = true;
			this.btm1Ubaci.Click += new System.EventHandler(this.btm1Ubaci_Click);
			// 
			// tb3Kolicina
			// 
			this.tb3Kolicina.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tb3Kolicina.Location = new System.Drawing.Point(401, 28);
			this.tb3Kolicina.Margin = new System.Windows.Forms.Padding(2);
			this.tb3Kolicina.MaxLength = 10;
			this.tb3Kolicina.Name = "tb3Kolicina";
			this.tb3Kolicina.Size = new System.Drawing.Size(46, 20);
			this.tb3Kolicina.TabIndex = 6;
			this.tb3Kolicina.Text = "Kolicina";
			this.tb3Kolicina.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tb1UnesiImeProizvoda_MouseClick);
			// 
			// tb2Cena
			// 
			this.tb2Cena.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tb2Cena.Location = new System.Drawing.Point(338, 28);
			this.tb2Cena.Margin = new System.Windows.Forms.Padding(2);
			this.tb2Cena.MaxLength = 10;
			this.tb2Cena.Name = "tb2Cena";
			this.tb2Cena.Size = new System.Drawing.Size(50, 20);
			this.tb2Cena.TabIndex = 5;
			this.tb2Cena.Text = "Cena";
			this.tb2Cena.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tb1UnesiImeProizvoda_MouseClick);
			// 
			// tb1UnesiImeProizvoda
			// 
			this.tb1UnesiImeProizvoda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tb1UnesiImeProizvoda.Location = new System.Drawing.Point(64, 28);
			this.tb1UnesiImeProizvoda.Margin = new System.Windows.Forms.Padding(2);
			this.tb1UnesiImeProizvoda.MaxLength = 20;
			this.tb1UnesiImeProizvoda.Name = "tb1UnesiImeProizvoda";
			this.tb1UnesiImeProizvoda.Size = new System.Drawing.Size(261, 20);
			this.tb1UnesiImeProizvoda.TabIndex = 4;
			this.tb1UnesiImeProizvoda.Text = "Unesi naziv proizvoda";
			this.tb1UnesiImeProizvoda.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tb1UnesiImeProizvoda_MouseClick);
			// 
			// dataGridViewProizvoda
			// 
			this.dataGridViewProizvoda.AllowUserToAddRows = false;
			this.dataGridViewProizvoda.AllowUserToDeleteRows = false;
			this.dataGridViewProizvoda.BackgroundColor = System.Drawing.Color.Coral;
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ActiveBorder;
			dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewProizvoda.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
			this.dataGridViewProizvoda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ScrollBar;
			dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewProizvoda.DefaultCellStyle = dataGridViewCellStyle5;
			this.dataGridViewProizvoda.GridColor = System.Drawing.Color.DimGray;
			this.dataGridViewProizvoda.Location = new System.Drawing.Point(64, 66);
			this.dataGridViewProizvoda.Margin = new System.Windows.Forms.Padding(2);
			this.dataGridViewProizvoda.Name = "dataGridViewProizvoda";
			this.dataGridViewProizvoda.ReadOnly = true;
			dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ActiveBorder;
			dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewProizvoda.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
			this.dataGridViewProizvoda.RowTemplate.Height = 24;
			this.dataGridViewProizvoda.Size = new System.Drawing.Size(397, 109);
			this.dataGridViewProizvoda.TabIndex = 3;
			// 
			// btmPOTVRDI
			// 
			this.btmPOTVRDI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.btmPOTVRDI.Location = new System.Drawing.Point(408, 500);
			this.btmPOTVRDI.Margin = new System.Windows.Forms.Padding(2);
			this.btmPOTVRDI.Name = "btmPOTVRDI";
			this.btmPOTVRDI.Size = new System.Drawing.Size(120, 25);
			this.btmPOTVRDI.TabIndex = 10;
			this.btmPOTVRDI.Text = "Potvrdi";
			this.btmPOTVRDI.UseVisualStyleBackColor = true;
			this.btmPOTVRDI.Click += new System.EventHandler(this.btmPOTVRDI_Click);
			// 
			// btmOtkazi
			// 
			this.btmOtkazi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.btmOtkazi.Location = new System.Drawing.Point(231, 500);
			this.btmOtkazi.Margin = new System.Windows.Forms.Padding(2);
			this.btmOtkazi.Name = "btmOtkazi";
			this.btmOtkazi.Size = new System.Drawing.Size(120, 25);
			this.btmOtkazi.TabIndex = 9;
			this.btmOtkazi.Text = "Otkazi";
			this.btmOtkazi.UseVisualStyleBackColor = true;
			this.btmOtkazi.Click += new System.EventHandler(this.btmOtkazi_Click);
			// 
			// Frm2PapirNarucivanja
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.HotTrack;
			this.ClientSize = new System.Drawing.Size(760, 546);
			this.Controls.Add(this.cb1Stanje);
			this.Controls.Add(this.lb8Stanje);
			this.Controls.Add(this.GrStavkeNarudzbine);
			this.Controls.Add(this.btmPOTVRDI);
			this.Controls.Add(this.btmOtkazi);
			this.Controls.Add(this.gB1Podaci);
			this.MaximumSize = new System.Drawing.Size(776, 585);
			this.MinimumSize = new System.Drawing.Size(776, 585);
			this.Name = "Frm2PapirNarucivanja";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Papir";
			this.gB1Podaci.ResumeLayout(false);
			this.gB1Podaci.PerformLayout();
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel3.PerformLayout();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.GrStavkeNarudzbine.ResumeLayout(false);
			this.GrStavkeNarudzbine.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProizvoda)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox gB1Podaci;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.Label lb7Potrosac;
		private System.Windows.Forms.TextBox tb1Potrosac;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Label lb6CenaPrevoza;
		private System.Windows.Forms.Label lb5Prevoznik;
		private System.Windows.Forms.Label lb4DatumIsporuke;
		private System.Windows.Forms.Label lb3DatumNajkIsporuke;
		private System.Windows.Forms.DateTimePicker dT2NajkDanIsporuke;
		private System.Windows.Forms.DateTimePicker dT4DatumIsporuke;
		private System.Windows.Forms.TextBox tb5Prevoznik;
		private System.Windows.Forms.TextBox tb6CenaPrevoza;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label lb1IDNaruzbenice;
		private System.Windows.Forms.Label lb2DatumPorudzbine;
		private System.Windows.Forms.TextBox tbIDNarudzbin1;
		private System.Windows.Forms.DateTimePicker dT1DatumPorudzbine;
		private System.Windows.Forms.ComboBox cb1Stanje;
		private System.Windows.Forms.Label lb8Stanje;
		private System.Windows.Forms.GroupBox GrStavkeNarudzbine;
		private System.Windows.Forms.Button btm2Izbaci;
		private System.Windows.Forms.Button btm1Ubaci;
		private System.Windows.Forms.TextBox tb3Kolicina;
		private System.Windows.Forms.TextBox tb2Cena;
		private System.Windows.Forms.TextBox tb1UnesiImeProizvoda;
		protected System.Windows.Forms.DataGridView dataGridViewProizvoda;
		private System.Windows.Forms.Button btmPOTVRDI;
		private System.Windows.Forms.Button btmOtkazi;
		private System.Windows.Forms.Label lblFirma;
	}
}