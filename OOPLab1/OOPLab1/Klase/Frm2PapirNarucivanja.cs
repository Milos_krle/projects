﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOPLab1
{
	public partial class Frm2PapirNarucivanja : Form
	{
		private Narudzbina narudzbinaZaIzmenu = null;

		public Narudzbina NarudzbinaZaIzmenu { get => narudzbinaZaIzmenu; set => narudzbinaZaIzmenu = value; }

		public Frm2PapirNarucivanja()
		{
			InitializeComponent();
			MojaInicijalizacija();
		}
		public Frm2PapirNarucivanja(Narudzbina nar)
		{
			narudzbinaZaIzmenu = nar;
			MojaInicijalizacija();
		}

		private void MojaInicijalizacija()
		{
			cb1Stanje.DataSource = Enum.GetNames(typeof(StanjaNarudzbine));
			dataGridViewProizvoda.Columns.Add("Ime proizvoda", "Ime proizvoda");
			dataGridViewProizvoda.Columns.Add("Cena", "Cena");
			dataGridViewProizvoda.Columns.Add("Kolicina", "Kolicina");
			dataGridViewProizvoda.AllowUserToAddRows = false;
			btm2Izbaci.Enabled = false;
		}

		
		private void InicijalizacijaZaIzmenu()
		{
			tbIDNarudzbin1.Text = NarudzbinaZaIzmenu.JedBrNarudzbine1.ToString();
			dT1DatumPorudzbine.Value = NarudzbinaZaIzmenu.DatumNarucivanja1;
			dT2NajkDanIsporuke.Value = NarudzbinaZaIzmenu.DatumNajkasnijeIsporuke1;
			dT4DatumIsporuke.Value = NarudzbinaZaIzmenu.DatumIsporuke1;
			tb5Prevoznik.Text = NarudzbinaZaIzmenu.Transporter;
			tb2Cena.Text = NarudzbinaZaIzmenu.CenaProizvoda.ToString();
			tb3Kolicina.Text = NarudzbinaZaIzmenu.KolicinaProizvoda.ToString();
			tb1Potrosac.Text = NarudzbinaZaIzmenu.Kupac;

			foreach (Proizvod p in NarudzbinaZaIzmenu.NizProizvoda)
			{
				dataGridViewProizvoda.Rows.Add(p.ImeProizvoda, p.CenaProizvoda.ToString(), p.KolicinaProizvoda.ToString());
			}
		}



		private void btmOtkazi_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btm2Izbaci_Click(object sender, EventArgs e)
		{
			DialogResult dr = MessageBox.Show("Da li ste sigurni da zelite da obrisete?", "Upozorenje", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (dr == DialogResult.Yes)
			{
				dataGridViewProizvoda.Rows.Remove(dataGridViewProizvoda.SelectedRows[0]);
			}
		}

		private void btm1Ubaci_Click(object sender, EventArgs e)
		{
			dataGridViewProizvoda.Rows.Add();
		}

		private void tbIDNarudzbin1_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar))
			{
				e.Handled = false;
				return;
			}
			e.Handled = true;
		}

		private void btmPOTVRDI_Click(object sender, EventArgs e)
		{
			
		}

		private void PotvrdiDugme()
		{
			bool pom = true;
			Narudzbina narudzbina1 = new Narudzbina();
			if (narudzbinaZaIzmenu != null)
			{
				pom = false;
				narudzbina1 = NarudzbinaZaIzmenu;
				narudzbina1.NizProizvoda.Clear();
			}
			narudzbina1.JedBrNarudzbine1 = tbIDNarudzbin1.Text;
			narudzbina1.DatumNarucivanja1 = dT1DatumPorudzbine.Value;
			narudzbina1.DatumNajkasnijeIsporuke1 = dT2NajkDanIsporuke.Value;
			narudzbina1.DatumIsporuke1 = dT4DatumIsporuke.Value;
			narudzbina1.Transporter = tb5Prevoznik.Text;
			if (tb6CenaPrevoza.Text != String.Empty)
			{
				narudzbina1.CenaProizvoda = decimal.Parse(tb6CenaPrevoza.Text);
			}
			
			narudzbina1.Kupac = tb1Potrosac.Text;

			foreach(DataGridViewRow red in dataGridViewProizvoda.Rows)
			{
				Proizvod proizv = new Proizvod();
				proizv.ImeProizvoda = red.Cells[0].Value.ToString();
				proizv.CenaProizvoda = decimal.Parse(red.Cells[1].Value.ToString());
				proizv.KolicinaProizvoda = UInt32.Parse(red.Cells[2].Value.ToString());
				narudzbina1.DodajProizvod(proizv);
			}
			decimal ukcen = narudzbina1.UkupnaSuma();
			if (pom)
			{
				((lbl2DonjaGranicaDatuma)Owner).Prodavnica.NizNarudzbina.Add(narudzbina1);
			}
			
			
		}

		private void tb1UnesiImeProizvoda_MouseClick(object sender, MouseEventArgs e)
		{
			tb1UnesiImeProizvoda.Clear();
		}
	}
}
