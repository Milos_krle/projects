﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPLab1
{
	public enum StanjaNarudzbine { Ispunjavanje, Obrada, Kompletan }
	public class Narudzbina : Proizvod, IUkupnaCena, IUlaz_Izlaz
	{
		private string JedBrNarudzbine;
		private DateTime DatumNarucivanja;
		private DateTime DatumNajkasnijeIsporuke;
		private DateTime DatumIsporuke;
		private List<Proizvod> nizProizvoda;
		private string transporter;
		private string kupac;
		private int brojProiz=0;
		private StanjaNarudzbine stanjaNarudzbine;
		
		

		public Narudzbina()
		{

			this.JedBrNarudzbine1 = "";
			StanjaNarudzbine = StanjaNarudzbine.Ispunjavanje;
			nizProizvoda = new List<Proizvod>();
			
		}
		public Narudzbina(string JedBrNar,DateTime narucivanje,DateTime danNajIsp, DateTime datIsporuke,string potrosac, string prev, int stanj)
		{
			this.JedBrNarudzbine1 = JedBrNar;
			this.DatumNarucivanja = narucivanje;
			this.DatumNajkasnijeIsporuke = danNajIsp;
			this.DatumIsporuke = datIsporuke;
			this.Kupac = potrosac;
			this.Transporter = prev;
			switch(stanj)
			{
			
				case 1:
					{
						this.stanjaNarudzbine = StanjaNarudzbine.Obrada;
						break;
					}
				case 2:
					{
						this.stanjaNarudzbine = StanjaNarudzbine.Kompletan;
						break;
					}
				default:
					{
						this.stanjaNarudzbine = StanjaNarudzbine.Ispunjavanje;
						break;
					}
			}
		}

		
		public DateTime DatumNarucivanja1 { get => DatumNarucivanja; set => DatumNarucivanja = value; }
		public StanjaNarudzbine StanjaNarudzbine { get => stanjaNarudzbine; set => stanjaNarudzbine = value; }
		
		public List<Proizvod> NizProizvoda { get => nizProizvoda; set => nizProizvoda = value; }
		public string Transporter { get => transporter; set => transporter = value; }
		public string Kupac { get => kupac; set => kupac = value; }
		public int BrojProiz { get => brojProiz; set => brojProiz = value; }
		public string JedBrNarudzbine1 { get => JedBrNarudzbine; set => JedBrNarudzbine = value; }
		public DateTime DatumNajkasnijeIsporuke1 { get => DatumNajkasnijeIsporuke; set => DatumNajkasnijeIsporuke = value; }
		public DateTime DatumIsporuke1 { get => DatumIsporuke; set => DatumIsporuke = value; }

		public decimal UkupnaSuma()
		{
			decimal ukupna=0;
			foreach(var i in nizProizvoda)
			{
				ukupna += i.CenaProizvoda * i.KolicinaProizvoda;
			}
			return ukupna;
		}

		public void DodajProizvod(Proizvod pro)
		{
			nizProizvoda.Add(pro);
		}
		public void ObrisiProizvod(int index)
		{
			nizProizvoda.RemoveAt(index);
		}

		public  void Ucitaj_iz_Fajl(StreamReader sr)
		{
			this.JedBrNarudzbine1 = sr.ReadLine();
			this.DatumNarucivanja = DateTime.Parse(sr.ReadLine());
			this.brojProiz = Int32.Parse(sr.ReadLine());
			for (int i=0; i<BrojProiz; i++)
			{
				nizProizvoda[i].Ucitaj_iz_Fajl(sr);
			}
			this.Transporter = sr.ReadLine();
			this.Kupac = sr.ReadLine();
			int stanj = Int32.Parse(sr.ReadLine());
			switch (stanj)
			{
				case 1:
					{
						this.stanjaNarudzbine = StanjaNarudzbine.Obrada;
						break;
					}
				case 2:
					{
						this.stanjaNarudzbine = StanjaNarudzbine.Kompletan;
						break;
					}
				default:
					{
						this.stanjaNarudzbine = StanjaNarudzbine.Ispunjavanje;
						break;
					}
			}
		}

		public  void Upisi_u_Fajl(StreamWriter sw)
		{
			this.BrojProiz = nizProizvoda.Capacity;
			sw.WriteLine(this.JedBrNarudzbine1);
			sw.WriteLine(DatumNarucivanja.ToString());
			sw.WriteLine(this.nizProizvoda.Count);
			foreach(Proizvod i in nizProizvoda)
			{
				i.Upisi_u_Fajl(sw);
			}
			sw.WriteLine(this.Transporter);
			sw.WriteLine(this.Kupac);
			int a;
			switch (stanjaNarudzbine)
			{
				
				case StanjaNarudzbine.Obrada:
					{
						a = 1;
						sw.WriteLine(a.ToString());
						break;
					}
				case StanjaNarudzbine.Kompletan:
					{
						a = 2;
						sw.WriteLine(a.ToString());
						break;
					}
				default:
					{
						a = 0;
						sw.WriteLine(a.ToString());
						break;
					}
			}
		}
	}
}

