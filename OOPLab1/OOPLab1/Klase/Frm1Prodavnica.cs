﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace OOPLab1
{
	public partial class lbl2DonjaGranicaDatuma : Form
	{

		private ListaNarudzbine prodavnica = null;

		internal ListaNarudzbine Prodavnica { get => prodavnica; set => prodavnica = value; }

		public lbl2DonjaGranicaDatuma()
		{
			InitializeComponent();
			InicijalizujeNarudzbine();
			dTP1DatumDonjeGranice.CustomFormat = "dd/MM/yyyy";
			dTP2DatumGornjeGranice.CustomFormat = "dd/MM/yyyy";
			tb1IDNarudzbine.Enabled = dTP1DatumDonjeGranice.Enabled = dTP2DatumGornjeGranice.Enabled = comboBoxStatus.Enabled = false;
			dGW1Prikaz.ReadOnly = true;

		}
		public void InicijalizujeNarudzbine()
		{
			Prodavnica = ListaNarudzbine.ListNarudzbinee;
		}
		private void izadjiToolStripMenuItem_Click(object sender, EventArgs e)
		{
			DialogResult dr = MessageBox.Show("Da li ste sigurni da zelite da izdjete?", "Upozorenje", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (dr == DialogResult.Yes)
			{
				Application.Exit();
			}
		}
		

		private void btn1Dodaj_Click(object sender, EventArgs e)
		{
			Form papir = new Frm2PapirNarucivanja();
			papir.Owner = this;
			papir.ShowDialog();
			
		}
		
	

		private void sFileDi1SacuvajRed_FileOk(object sender, CancelEventArgs e)
		{

		}
		public void Dozvoli()
		{
			tb1IDNarudzbine.Enabled = dTP1DatumDonjeGranice.Enabled = dTP2DatumGornjeGranice.Enabled = comboBoxStatus.Enabled = true;
		}

		private void sFielD2SacuvajUfajl_FileOk(object sender, CancelEventArgs e)
		{
			if (sFielD2SacuvajUfajl.ShowDialog() == DialogResult.OK)
			{
				
			}
		}

		private void openFileD1Ucitaj_FileOk(object sender, CancelEventArgs e)
		{

		}
		
		private void btn2Uredi_Click(object sender, EventArgs e)
		{
			DataGridViewRow ro = dGW1Prikaz.SelectedRows[0];
			string id = ro.Cells[0].Value.ToString();
			Narudzbina nar = Prodavnica.PronadjiNarudzbinu(id);
			var papir = new Frm2PapirNarucivanja();
			papir.Owner = this;
			papir.ShowDialog();
		}
		private void PrimeniNaGrid()
		{

		}

		private void btn3Obrisi_Click(object sender, EventArgs e)
		{
			DialogResult dr = MessageBox.Show("Da li ste sigurni da zelite da obrisete?", "Upozorenje", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (dr == DialogResult.Yes)
			{
				string inde = dGW1Prikaz.CurrentRow.Cells[0].Value.ToString();
				dGW1Prikaz.Rows.Remove(dGW1Prikaz.CurrentRow);
				Narudzbina nar = ListaNarudzbine.ListNarudzbinee.PronadjiNarudzbinu(inde);

			}
		}

		private void btn2PonistiFilter_Click(object sender, EventArgs e)
		{
			this.tb1IDNarudzbine.Clear();
			this.dTP1DatumDonjeGranice.Value = dTP1DatumDonjeGranice.MinDate;
			this.dTP2DatumGornjeGranice.Enabled = false;
			this.dGW1Prikaz.Refresh();
		}

		private void tb1IDNarudzbine_MouseClick(object sender, MouseEventArgs e)
		{
			this.tb1IDNarudzbine.Clear();
		}
	}
}
