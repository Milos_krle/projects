﻿namespace OOPLab1
{
	partial class lbl2DonjaGranicaDatuma
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.menuStrip1Meni = new System.Windows.Forms.MenuStrip();
			this.fajlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.sacuvajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.izadjiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.oAplikacijiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.prikazToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.dGW1Prikaz = new System.Windows.Forms.DataGridView();
			this.ClmNarudzbenica = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.clm2Kupljeno_na = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.cm3RacunNaImenu = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.cm4DostavljanjeNaIme = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.clm5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.clm6Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.gB1Filters = new System.Windows.Forms.GroupBox();
			this.lbl4Status2Narudzbine = new System.Windows.Forms.Label();
			this.lbl3GornjaGraniaDat = new System.Windows.Forms.Label();
			this.lbl2DonjaGranicaDat = new System.Windows.Forms.Label();
			this.lbl1IDBrojNarudzbine = new System.Windows.Forms.Label();
			this.lb1Opis = new System.Windows.Forms.Label();
			this.btn2PonistiFilter = new System.Windows.Forms.Button();
			this.btn1Filtriraj = new System.Windows.Forms.Button();
			this.comboBoxStatus = new System.Windows.Forms.ComboBox();
			this.dTP2DatumGornjeGranice = new System.Windows.Forms.DateTimePicker();
			this.dTP1DatumDonjeGranice = new System.Windows.Forms.DateTimePicker();
			this.tb1IDNarudzbine = new System.Windows.Forms.TextBox();
			this.gB2Stavke = new System.Windows.Forms.GroupBox();
			this.btn7IzveziUFajl = new System.Windows.Forms.Button();
			this.btn6Kompletan = new System.Windows.Forms.Button();
			this.btn5Obrada = new System.Windows.Forms.Button();
			this.btn4Ispunjenje = new System.Windows.Forms.Button();
			this.btn3Obrisi = new System.Windows.Forms.Button();
			this.btn2Uredi = new System.Windows.Forms.Button();
			this.btn1Dodaj = new System.Windows.Forms.Button();
			this.gB3List = new System.Windows.Forms.GroupBox();
			this.btn9Izvezi = new System.Windows.Forms.Button();
			this.btn8Uvezi = new System.Windows.Forms.Button();
			this.sFileDi1SacuvajRed = new System.Windows.Forms.SaveFileDialog();
			this.sFielD2SacuvajUfajl = new System.Windows.Forms.SaveFileDialog();
			this.openFileD1Ucitaj = new System.Windows.Forms.OpenFileDialog();
			this.menuStrip1Meni.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dGW1Prikaz)).BeginInit();
			this.gB1Filters.SuspendLayout();
			this.gB2Stavke.SuspendLayout();
			this.gB3List.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1Meni
			// 
			this.menuStrip1Meni.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.menuStrip1Meni.BackColor = System.Drawing.SystemColors.HotTrack;
			this.menuStrip1Meni.Dock = System.Windows.Forms.DockStyle.None;
			this.menuStrip1Meni.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fajlToolStripMenuItem,
            this.oAplikacijiToolStripMenuItem});
			this.menuStrip1Meni.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1Meni.Name = "menuStrip1Meni";
			this.menuStrip1Meni.Size = new System.Drawing.Size(124, 24);
			this.menuStrip1Meni.TabIndex = 0;
			this.menuStrip1Meni.Text = "Menu";
			// 
			// fajlToolStripMenuItem
			// 
			this.fajlToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sacuvajToolStripMenuItem,
            this.izadjiToolStripMenuItem});
			this.fajlToolStripMenuItem.Name = "fajlToolStripMenuItem";
			this.fajlToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fajlToolStripMenuItem.Text = "Fajl";
			// 
			// sacuvajToolStripMenuItem
			// 
			this.sacuvajToolStripMenuItem.Name = "sacuvajToolStripMenuItem";
			this.sacuvajToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.sacuvajToolStripMenuItem.Text = "Sacuvaj";
			// 
			// izadjiToolStripMenuItem
			// 
			this.izadjiToolStripMenuItem.Name = "izadjiToolStripMenuItem";
			this.izadjiToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.izadjiToolStripMenuItem.Text = "Izadji";
			this.izadjiToolStripMenuItem.Click += new System.EventHandler(this.izadjiToolStripMenuItem_Click);
			// 
			// oAplikacijiToolStripMenuItem
			// 
			this.oAplikacijiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prikazToolStripMenuItem});
			this.oAplikacijiToolStripMenuItem.Name = "oAplikacijiToolStripMenuItem";
			this.oAplikacijiToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
			this.oAplikacijiToolStripMenuItem.Text = "O Aplikaciji";
			// 
			// prikazToolStripMenuItem
			// 
			this.prikazToolStripMenuItem.Name = "prikazToolStripMenuItem";
			this.prikazToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
			this.prikazToolStripMenuItem.Text = "Prikaz";
			// 
			// dGW1Prikaz
			// 
			this.dGW1Prikaz.AllowUserToOrderColumns = true;
			this.dGW1Prikaz.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dGW1Prikaz.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader;
			this.dGW1Prikaz.BackgroundColor = System.Drawing.Color.LightBlue;
			this.dGW1Prikaz.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
			this.dGW1Prikaz.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dGW1Prikaz.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ClmNarudzbenica,
            this.clm2Kupljeno_na,
            this.cm3RacunNaImenu,
            this.cm4DostavljanjeNaIme,
            this.clm5,
            this.clm6Status});
			this.dGW1Prikaz.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.dGW1Prikaz.GridColor = System.Drawing.Color.Blue;
			this.dGW1Prikaz.Location = new System.Drawing.Point(0, 120);
			this.dGW1Prikaz.Name = "dGW1Prikaz";
			this.dGW1Prikaz.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dGW1Prikaz.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dGW1Prikaz.Size = new System.Drawing.Size(1014, 342);
			this.dGW1Prikaz.TabIndex = 1;
			// 
			// ClmNarudzbenica
			// 
			this.ClmNarudzbenica.HeaderText = " ID Narudzbenice #";
			this.ClmNarudzbenica.MinimumWidth = 160;
			this.ClmNarudzbenica.Name = "ClmNarudzbenica";
			this.ClmNarudzbenica.Width = 160;
			// 
			// clm2Kupljeno_na
			// 
			this.clm2Kupljeno_na.HeaderText = "Kupljeno na";
			this.clm2Kupljeno_na.MinimumWidth = 160;
			this.clm2Kupljeno_na.Name = "clm2Kupljeno_na";
			this.clm2Kupljeno_na.Width = 160;
			// 
			// cm3RacunNaImenu
			// 
			this.cm3RacunNaImenu.HeaderText = "Racun na imenu";
			this.cm3RacunNaImenu.MinimumWidth = 160;
			this.cm3RacunNaImenu.Name = "cm3RacunNaImenu";
			this.cm3RacunNaImenu.Width = 160;
			// 
			// cm4DostavljanjeNaIme
			// 
			this.cm4DostavljanjeNaIme.HeaderText = "Dostava na ime";
			this.cm4DostavljanjeNaIme.MinimumWidth = 170;
			this.cm4DostavljanjeNaIme.Name = "cm4DostavljanjeNaIme";
			this.cm4DostavljanjeNaIme.Width = 170;
			// 
			// clm5
			// 
			this.clm5.HeaderText = "Prihod";
			this.clm5.MinimumWidth = 160;
			this.clm5.Name = "clm5";
			this.clm5.Width = 160;
			// 
			// clm6Status
			// 
			this.clm6Status.HeaderText = "Status";
			this.clm6Status.MinimumWidth = 160;
			this.clm6Status.Name = "clm6Status";
			this.clm6Status.Width = 160;
			// 
			// gB1Filters
			// 
			this.gB1Filters.BackColor = System.Drawing.Color.LightGoldenrodYellow;
			this.gB1Filters.Controls.Add(this.lbl4Status2Narudzbine);
			this.gB1Filters.Controls.Add(this.lbl3GornjaGraniaDat);
			this.gB1Filters.Controls.Add(this.lbl2DonjaGranicaDat);
			this.gB1Filters.Controls.Add(this.lbl1IDBrojNarudzbine);
			this.gB1Filters.Controls.Add(this.lb1Opis);
			this.gB1Filters.Controls.Add(this.btn2PonistiFilter);
			this.gB1Filters.Controls.Add(this.btn1Filtriraj);
			this.gB1Filters.Controls.Add(this.comboBoxStatus);
			this.gB1Filters.Controls.Add(this.dTP2DatumGornjeGranice);
			this.gB1Filters.Controls.Add(this.dTP1DatumDonjeGranice);
			this.gB1Filters.Controls.Add(this.tb1IDNarudzbine);
			this.gB1Filters.Location = new System.Drawing.Point(0, 27);
			this.gB1Filters.Name = "gB1Filters";
			this.gB1Filters.Size = new System.Drawing.Size(1014, 87);
			this.gB1Filters.TabIndex = 4;
			this.gB1Filters.TabStop = false;
			this.gB1Filters.Text = "Filtri";
			// 
			// lbl4Status2Narudzbine
			// 
			this.lbl4Status2Narudzbine.AutoSize = true;
			this.lbl4Status2Narudzbine.Location = new System.Drawing.Point(621, 20);
			this.lbl4Status2Narudzbine.Name = "lbl4Status2Narudzbine";
			this.lbl4Status2Narudzbine.Size = new System.Drawing.Size(92, 13);
			this.lbl4Status2Narudzbine.TabIndex = 18;
			this.lbl4Status2Narudzbine.Text = "Status narudzbine";
			// 
			// lbl3GornjaGraniaDat
			// 
			this.lbl3GornjaGraniaDat.AutoSize = true;
			this.lbl3GornjaGraniaDat.Location = new System.Drawing.Point(428, 21);
			this.lbl3GornjaGraniaDat.Name = "lbl3GornjaGraniaDat";
			this.lbl3GornjaGraniaDat.Size = new System.Drawing.Size(114, 13);
			this.lbl3GornjaGraniaDat.TabIndex = 17;
			this.lbl3GornjaGraniaDat.Text = "Gornja granica datuma";
			// 
			// lbl2DonjaGranicaDat
			// 
			this.lbl2DonjaGranicaDat.AutoSize = true;
			this.lbl2DonjaGranicaDat.Location = new System.Drawing.Point(235, 21);
			this.lbl2DonjaGranicaDat.Name = "lbl2DonjaGranicaDat";
			this.lbl2DonjaGranicaDat.Size = new System.Drawing.Size(111, 13);
			this.lbl2DonjaGranicaDat.TabIndex = 16;
			this.lbl2DonjaGranicaDat.Text = "Donja granica datuma";
			// 
			// lbl1IDBrojNarudzbine
			// 
			this.lbl1IDBrojNarudzbine.AutoSize = true;
			this.lbl1IDBrojNarudzbine.Location = new System.Drawing.Point(56, 21);
			this.lbl1IDBrojNarudzbine.Name = "lbl1IDBrojNarudzbine";
			this.lbl1IDBrojNarudzbine.Size = new System.Drawing.Size(93, 13);
			this.lbl1IDBrojNarudzbine.TabIndex = 15;
			this.lbl1IDBrojNarudzbine.Text = "ID broj narudzbine";
			// 
			// lb1Opis
			// 
			this.lb1Opis.AutoSize = true;
			this.lb1Opis.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lb1Opis.ForeColor = System.Drawing.Color.Maroon;
			this.lb1Opis.Location = new System.Drawing.Point(718, 17);
			this.lb1Opis.Name = "lb1Opis";
			this.lb1Opis.Size = new System.Drawing.Size(239, 19);
			this.lb1Opis.TabIndex = 10;
			this.lb1Opis.Text = "Unesite po cemu bi vrsili filtriranje";
			// 
			// btn2PonistiFilter
			// 
			this.btn2PonistiFilter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
			this.btn2PonistiFilter.Location = new System.Drawing.Point(876, 46);
			this.btn2PonistiFilter.Name = "btn2PonistiFilter";
			this.btn2PonistiFilter.Size = new System.Drawing.Size(92, 31);
			this.btn2PonistiFilter.TabIndex = 9;
			this.btn2PonistiFilter.Text = "Ponisti filtriranje";
			this.btn2PonistiFilter.UseVisualStyleBackColor = false;
			this.btn2PonistiFilter.Click += new System.EventHandler(this.btn2PonistiFilter_Click);
			// 
			// btn1Filtriraj
			// 
			this.btn1Filtriraj.BackColor = System.Drawing.SystemColors.HotTrack;
			this.btn1Filtriraj.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.btn1Filtriraj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn1Filtriraj.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.btn1Filtriraj.Location = new System.Drawing.Point(722, 46);
			this.btn1Filtriraj.Name = "btn1Filtriraj";
			this.btn1Filtriraj.Size = new System.Drawing.Size(112, 31);
			this.btn1Filtriraj.TabIndex = 8;
			this.btn1Filtriraj.Text = "Filtriraj";
			this.btn1Filtriraj.UseVisualStyleBackColor = false;
			// 
			// comboBoxStatus
			// 
			this.comboBoxStatus.FormattingEnabled = true;
			this.comboBoxStatus.Items.AddRange(new object[] {
            "Ispunjavanje",
            "Obrada",
            "Kompletan"});
			this.comboBoxStatus.Location = new System.Drawing.Point(604, 41);
			this.comboBoxStatus.Margin = new System.Windows.Forms.Padding(2);
			this.comboBoxStatus.Name = "comboBoxStatus";
			this.comboBoxStatus.Size = new System.Drawing.Size(92, 21);
			this.comboBoxStatus.TabIndex = 4;
			this.comboBoxStatus.Text = "Status";
			// 
			// dTP2DatumGornjeGranice
			// 
			this.dTP2DatumGornjeGranice.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dTP2DatumGornjeGranice.Location = new System.Drawing.Point(431, 43);
			this.dTP2DatumGornjeGranice.Name = "dTP2DatumGornjeGranice";
			this.dTP2DatumGornjeGranice.Size = new System.Drawing.Size(95, 20);
			this.dTP2DatumGornjeGranice.TabIndex = 3;
			// 
			// dTP1DatumDonjeGranice
			// 
			this.dTP1DatumDonjeGranice.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dTP1DatumDonjeGranice.Location = new System.Drawing.Point(238, 43);
			this.dTP1DatumDonjeGranice.Name = "dTP1DatumDonjeGranice";
			this.dTP1DatumDonjeGranice.Size = new System.Drawing.Size(95, 20);
			this.dTP1DatumDonjeGranice.TabIndex = 2;
			// 
			// tb1IDNarudzbine
			// 
			this.tb1IDNarudzbine.Location = new System.Drawing.Point(49, 46);
			this.tb1IDNarudzbine.Name = "tb1IDNarudzbine";
			this.tb1IDNarudzbine.Size = new System.Drawing.Size(100, 20);
			this.tb1IDNarudzbine.TabIndex = 0;
			this.tb1IDNarudzbine.Text = "ID broj narudzbine";
			this.tb1IDNarudzbine.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tb1IDNarudzbine_MouseClick);
			// 
			// gB2Stavke
			// 
			this.gB2Stavke.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.gB2Stavke.BackColor = System.Drawing.Color.LightGoldenrodYellow;
			this.gB2Stavke.Controls.Add(this.btn7IzveziUFajl);
			this.gB2Stavke.Controls.Add(this.btn6Kompletan);
			this.gB2Stavke.Controls.Add(this.btn5Obrada);
			this.gB2Stavke.Controls.Add(this.btn4Ispunjenje);
			this.gB2Stavke.Controls.Add(this.btn3Obrisi);
			this.gB2Stavke.Controls.Add(this.btn2Uredi);
			this.gB2Stavke.Controls.Add(this.btn1Dodaj);
			this.gB2Stavke.Location = new System.Drawing.Point(0, 468);
			this.gB2Stavke.Name = "gB2Stavke";
			this.gB2Stavke.Size = new System.Drawing.Size(1014, 43);
			this.gB2Stavke.TabIndex = 5;
			this.gB2Stavke.TabStop = false;
			this.gB2Stavke.Text = "Stavke";
			// 
			// btn7IzveziUFajl
			// 
			this.btn7IzveziUFajl.Location = new System.Drawing.Point(876, 14);
			this.btn7IzveziUFajl.Name = "btn7IzveziUFajl";
			this.btn7IzveziUFajl.Size = new System.Drawing.Size(75, 23);
			this.btn7IzveziUFajl.TabIndex = 6;
			this.btn7IzveziUFajl.Text = "Izvezi u fajl";
			this.btn7IzveziUFajl.UseVisualStyleBackColor = true;
			// 
			// btn6Kompletan
			// 
			this.btn6Kompletan.Location = new System.Drawing.Point(722, 14);
			this.btn6Kompletan.Name = "btn6Kompletan";
			this.btn6Kompletan.Size = new System.Drawing.Size(75, 23);
			this.btn6Kompletan.TabIndex = 5;
			this.btn6Kompletan.Text = "Kompletan";
			this.btn6Kompletan.UseVisualStyleBackColor = true;
			// 
			// btn5Obrada
			// 
			this.btn5Obrada.Location = new System.Drawing.Point(604, 14);
			this.btn5Obrada.Name = "btn5Obrada";
			this.btn5Obrada.Size = new System.Drawing.Size(75, 23);
			this.btn5Obrada.TabIndex = 4;
			this.btn5Obrada.Text = "Obrada";
			this.btn5Obrada.UseVisualStyleBackColor = true;
			// 
			// btn4Ispunjenje
			// 
			this.btn4Ispunjenje.Location = new System.Drawing.Point(489, 14);
			this.btn4Ispunjenje.Name = "btn4Ispunjenje";
			this.btn4Ispunjenje.Size = new System.Drawing.Size(75, 23);
			this.btn4Ispunjenje.TabIndex = 3;
			this.btn4Ispunjenje.Text = "Ispunjavanje";
			this.btn4Ispunjenje.UseVisualStyleBackColor = true;
			// 
			// btn3Obrisi
			// 
			this.btn3Obrisi.Location = new System.Drawing.Point(268, 14);
			this.btn3Obrisi.Name = "btn3Obrisi";
			this.btn3Obrisi.Size = new System.Drawing.Size(75, 23);
			this.btn3Obrisi.TabIndex = 2;
			this.btn3Obrisi.Text = "Obrisi";
			this.btn3Obrisi.UseVisualStyleBackColor = true;
			this.btn3Obrisi.Click += new System.EventHandler(this.btn3Obrisi_Click);
			// 
			// btn2Uredi
			// 
			this.btn2Uredi.Location = new System.Drawing.Point(160, 14);
			this.btn2Uredi.Name = "btn2Uredi";
			this.btn2Uredi.Size = new System.Drawing.Size(75, 23);
			this.btn2Uredi.TabIndex = 1;
			this.btn2Uredi.Text = "Uredi";
			this.btn2Uredi.UseVisualStyleBackColor = true;
			this.btn2Uredi.Click += new System.EventHandler(this.btn2Uredi_Click);
			// 
			// btn1Dodaj
			// 
			this.btn1Dodaj.Location = new System.Drawing.Point(49, 14);
			this.btn1Dodaj.Name = "btn1Dodaj";
			this.btn1Dodaj.Size = new System.Drawing.Size(75, 23);
			this.btn1Dodaj.TabIndex = 0;
			this.btn1Dodaj.Text = "Dodaj";
			this.btn1Dodaj.UseVisualStyleBackColor = true;
			this.btn1Dodaj.Click += new System.EventHandler(this.btn1Dodaj_Click);
			// 
			// gB3List
			// 
			this.gB3List.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.gB3List.BackColor = System.Drawing.Color.LightGoldenrodYellow;
			this.gB3List.Controls.Add(this.btn9Izvezi);
			this.gB3List.Controls.Add(this.btn8Uvezi);
			this.gB3List.Location = new System.Drawing.Point(0, 517);
			this.gB3List.Name = "gB3List";
			this.gB3List.Size = new System.Drawing.Size(1014, 45);
			this.gB3List.TabIndex = 6;
			this.gB3List.TabStop = false;
			this.gB3List.Text = "List";
			// 
			// btn9Izvezi
			// 
			this.btn9Izvezi.Location = new System.Drawing.Point(219, 10);
			this.btn9Izvezi.Name = "btn9Izvezi";
			this.btn9Izvezi.Size = new System.Drawing.Size(75, 23);
			this.btn9Izvezi.TabIndex = 8;
			this.btn9Izvezi.Text = "Izvezi ";
			this.btn9Izvezi.UseVisualStyleBackColor = true;
			// 
			// btn8Uvezi
			// 
			this.btn8Uvezi.Location = new System.Drawing.Point(99, 10);
			this.btn8Uvezi.Name = "btn8Uvezi";
			this.btn8Uvezi.Size = new System.Drawing.Size(75, 23);
			this.btn8Uvezi.TabIndex = 7;
			this.btn8Uvezi.Text = "Uvezi";
			this.btn8Uvezi.UseVisualStyleBackColor = true;
			// 
			// sFileDi1SacuvajRed
			// 
			this.sFileDi1SacuvajRed.FileName = "SDileSacuvajRedFAjl";
			this.sFileDi1SacuvajRed.FileOk += new System.ComponentModel.CancelEventHandler(this.sFileDi1SacuvajRed_FileOk);
			// 
			// sFielD2SacuvajUfajl
			// 
			this.sFielD2SacuvajUfajl.FileName = "sFD2SacuvajFajl";
			this.sFielD2SacuvajUfajl.FileOk += new System.ComponentModel.CancelEventHandler(this.sFielD2SacuvajUfajl_FileOk);
			// 
			// openFileD1Ucitaj
			// 
			this.openFileD1Ucitaj.FileName = "openFileD1OtvoriFajl";
			this.openFileD1Ucitaj.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileD1Ucitaj_FileOk);
			// 
			// lbl2DonjaGranicaDatuma
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.SystemColors.Highlight;
			this.ClientSize = new System.Drawing.Size(1014, 568);
			this.Controls.Add(this.dGW1Prikaz);
			this.Controls.Add(this.gB3List);
			this.Controls.Add(this.gB2Stavke);
			this.Controls.Add(this.gB1Filters);
			this.Controls.Add(this.menuStrip1Meni);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.HelpButton = true;
			this.MainMenuStrip = this.menuStrip1Meni;
			this.MaximumSize = new System.Drawing.Size(1030, 607);
			this.MinimumSize = new System.Drawing.Size(1030, 607);
			this.Name = "lbl2DonjaGranicaDatuma";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Prodavnica";
			this.menuStrip1Meni.ResumeLayout(false);
			this.menuStrip1Meni.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dGW1Prikaz)).EndInit();
			this.gB1Filters.ResumeLayout(false);
			this.gB1Filters.PerformLayout();
			this.gB2Stavke.ResumeLayout(false);
			this.gB3List.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1Meni;
		private System.Windows.Forms.ToolStripMenuItem fajlToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem oAplikacijiToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem prikazToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem sacuvajToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem izadjiToolStripMenuItem;
		private System.Windows.Forms.DataGridView dGW1Prikaz;
		private System.Windows.Forms.GroupBox gB1Filters;
		private System.Windows.Forms.DateTimePicker dTP1DatumDonjeGranice;
		private System.Windows.Forms.TextBox tb1IDNarudzbine;
		private System.Windows.Forms.ComboBox comboBoxStatus;
		private System.Windows.Forms.DateTimePicker dTP2DatumGornjeGranice;
		private System.Windows.Forms.Button btn2PonistiFilter;
		private System.Windows.Forms.Button btn1Filtriraj;
		private System.Windows.Forms.Label lb1Opis;
		private System.Windows.Forms.Button btn3Obrisi;
		private System.Windows.Forms.Button btn2Uredi;
		private System.Windows.Forms.Button btn1Dodaj;
		private System.Windows.Forms.Button btn7IzveziUFajl;
		private System.Windows.Forms.Button btn6Kompletan;
		private System.Windows.Forms.Button btn5Obrada;
		private System.Windows.Forms.Button btn4Ispunjenje;
		private System.Windows.Forms.Button btn9Izvezi;
		private System.Windows.Forms.Button btn8Uvezi;
		private System.Windows.Forms.GroupBox gB2Stavke;
		private System.Windows.Forms.GroupBox gB3List;
		private System.Windows.Forms.DataGridViewTextBoxColumn ClmNarudzbenica;
		private System.Windows.Forms.DataGridViewTextBoxColumn clm2Kupljeno_na;
		private System.Windows.Forms.DataGridViewTextBoxColumn cm3RacunNaImenu;
		private System.Windows.Forms.DataGridViewTextBoxColumn cm4DostavljanjeNaIme;
		private System.Windows.Forms.DataGridViewTextBoxColumn clm5;
		private System.Windows.Forms.DataGridViewTextBoxColumn clm6Status;
		private System.Windows.Forms.SaveFileDialog sFileDi1SacuvajRed;
		private System.Windows.Forms.SaveFileDialog sFielD2SacuvajUfajl;
		private System.Windows.Forms.OpenFileDialog openFileD1Ucitaj;
		private System.Windows.Forms.Label lbl4Status2Narudzbine;
		private System.Windows.Forms.Label lbl3GornjaGraniaDat;
		private System.Windows.Forms.Label lbl2DonjaGranicaDat;
		private System.Windows.Forms.Label lbl1IDBrojNarudzbine;
	}
}

