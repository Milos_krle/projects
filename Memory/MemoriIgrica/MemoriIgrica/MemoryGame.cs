﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Podaci;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;

namespace MemoriIgrica
{

    public partial class MemoryGame : Form
    {
        #region Atributi
        const string imeFajla = "memGame.xml";
        private Matrica matrica;
        int minimalniBrParova = 10;
        int minimalniBrSlika = 5;
        int maksimalanBrParova = 100;
        int maksimalniBrSlika = 12;
        int maxX = 8;
        int maxY = 17;
        int minX = 6;
        int minY = 10;
        int minPolje = 60;
        int brSlika, brParova, x, y;
        int BrojKlikaNaSlike = 0;
        PictureBox poslednjiOtvorenPicBox;
        PictureBox trenutnoOtvoreniPicBox;
        List<string> nizVremena;
        int vreme;
        int VremeKlika;
        int vr = 0, kol = 0;
        int klikNaPodesi = 0;
        static int sacuvanFajl = 0;
        #endregion
        public MemoryGame()
        {
            InitializeComponent();
            MyInitialization();
        }
        private void MyInitialization()
        {
            x = minX;
            y = minY;
            brSlika = minimalniBrSlika;
            brParova = minimalniBrParova;
            trenutnoOtvoreniPicBox = null;
            poslednjiOtvorenPicBox = null;
            vreme = 0;
            VremeKlika = 0;
            nizVremena = new List<string>(); //pamti vremena

            NapraviNovuIgru();
        }
        #region Geteri
        public int MinX
        {
            get { return minX; }
        }
        public int MaxX
        {
            get { return maxX; }
        }
        public int MinY
        {
            get { return minY; }
        }
        public int MaxY
        {
            get { return maxY; }
        }
        public int MinPolje
        {
            get { return minPolje; }
        }
        public int MinimalanBrParova
        {
            get { return minimalniBrParova; }
        }
        public int MinimalanBrSlika
        {
            get { return minimalniBrSlika; }
        }
        public int MaksimalanBrParova
        {
            get { return maksimalanBrParova; }
        }
        public int MaksimalanBrSlika
        {
            get { return maksimalniBrSlika; }
        }
        public int MinimalnaVelicinaPolja
        {
            get { return minPolje; }
        }
        #endregion
        public int X
        {
            set { x = value; }
        }
        public int Y
        {
            set { y = value; }
        }
        public int BrSlika
        {
            set { brSlika = value; }
        }
        public int BrParova
        {
            set { brParova = value; }
        }
        public void NapraviNovuIgru() //
        {

            matrica = new Matrica(brParova, brSlika, x, y);
            trenutnoOtvoreniPicBox = null;
            poslednjiOtvorenPicBox = null;
            IzbrisiPicBoxove();
            KreirajPictureBox();
            timer.Start();
            vreme = 0;
        }
        private void prikaziPoljaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Da li ste sigurni da zelite da otvorite sva polja i zavrsite igru?", "Upozorenje",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                matrica.OtvoriSveSlike();
                timer.Stop();
                foreach (PictureBox pb in this.Controls.OfType<PictureBox>())
                {
                    string[] ime = pb.Name.Split(',');
                    int vrsta = int.Parse(ime[0]);
                    int kolona = int.Parse(ime[1]);
                    PostaviSlikuNaPB(pb, vrsta, kolona);
                }
            }
        }
        public void PicBoxKlik(Object pb, EventArgs e)
        {
            int i, j;
            string[] ime = ((PictureBox)pb).Name.Split(',');
            i = int.Parse(ime[0]);
            j = int.Parse(ime[1]);

            if (!matrica.DaLiJeZatvorena(i, j))
            {
                return;
            } //

            if (trenutnoOtvoreniPicBox != null && poslednjiOtvorenPicBox != null)
            {
                if (matrica.DaLiSuIste())
                {
                    matrica.OtvoriPoslednjuOtvorenu();
                    matrica.OtvoriTrenutnoOtvorenu();
                    trenutnoOtvoreniPicBox = null;
                    poslednjiOtvorenPicBox = null;
                }
                else
                {
                    poslednjiOtvorenPicBox.Image = matrica.ZatvoriPrethodnuSliku();
                    trenutnoOtvoreniPicBox.Image = matrica.ZatvoriTrenutnoOtvorenuSLiku();
                    trenutnoOtvoreniPicBox = null;
                    poslednjiOtvorenPicBox = null;
                }
                trenutnoOtvoreniPicBox = (PictureBox)pb;
                trenutnoOtvoreniPicBox.Image = matrica.PrivremenoOtvoriSliku(i, j);
                if (matrica.DaLiJePrazna(i, j)) //PROVERA ZA PRAZNU SLIKU
                {
                    matrica.OtvoriSliku(i, j);
                    trenutnoOtvoreniPicBox = null;
                }
                if (matrica.DaLiJeKrajIgre())
                {
                    KrajIgre();
                }
                return;
            }
            if (trenutnoOtvoreniPicBox == null)
            {
                trenutnoOtvoreniPicBox = (PictureBox)pb;
                trenutnoOtvoreniPicBox.Image = matrica.PrivremenoOtvoriSliku(i, j);
                if (matrica.DaLiJePrazna(i, j)) //PROVERA ZA PRAZNU SLIKU
                {
                    matrica.OtvoriSliku(i, j);
                    trenutnoOtvoreniPicBox = null;
                }
                if (matrica.DaLiJeKrajIgre())
                {
                    KrajIgre();
                }
                return;
            }
            else
            {
                poslednjiOtvorenPicBox = trenutnoOtvoreniPicBox;
                matrica.PromeniStanjeTrenutniPoslednji();
                trenutnoOtvoreniPicBox = (PictureBox)pb;
                trenutnoOtvoreniPicBox.Image = matrica.PrivremenoOtvoriSliku(i, j);
                if (matrica.DaLiJePrazna(i, j)) ///PROVERA ZA PRAZNU SLIKU
                {
                    matrica.OtvoriSliku(i, j);
                    trenutnoOtvoreniPicBox = null;
                    poslednjiOtvorenPicBox.Image = matrica.ZatvoriPrethodnuSliku();
                    poslednjiOtvorenPicBox = null;
                }
                if (matrica.DaLiJeKrajIgre())
                {
                    KrajIgre();
                }
                return;
            }
        }

        private void novaIgraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Da li ste sigurni da zelite da zapocnete novu igru?", "Upozorenje",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                NapraviNovuIgru();
                this.klikNaPodesi = 0;
            }
        }

        private void KreirajPictureBox()
        {
            Size sz = new Size(70, 70);
            int p = 0, q = 0;
            for (int i = 0; i < x; i++)
            {
                q = 0;
                for (int j = 0; j < y; j++)
                {
                    PictureBox pb = new PictureBox();
                    pb.Size = sz;
                    pb.Location = new Point(10 + q, 50 + p);
                    PostaviSlikuNaPB(pb, i, j);
                    pb.Name = i.ToString() + "," + j.ToString();
                    this.Controls.Add(pb);
                    pb.Click += PicBoxKlik;
                    q += 80;
                }
                p += 80;
            }
            sz = new Size(q + 30, p + 90);
            this.Size = sz;
            this.FormBorderStyle = FormBorderStyle.Fixed3D;
        }

        private void izadjiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Izaci iz igre?", "Upozorenje",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                DialogResult dr1 = MessageBox.Show("Da li zelite da sacuvate trenutno stanje?", "Upozorenje",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr1 == DialogResult.Yes)
                {
                    UpisiUFajl(x, y, brSlika, brParova);
                    ++sacuvanFajl;
                }
                this.Dispose();
            }

        }

        private void odaberiKonfiguracijuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ++klikNaPodesi;
           
            if (klikNaPodesi > 1)
            {
                PodesavanjeKonfiguracije form = new PodesavanjeKonfiguracije(x,y,brSlika,brParova);
                form.Owner = this;
                form.ShowDialog();
            }
            else
            {
                PodesavanjeKonfiguracije form = new PodesavanjeKonfiguracije();
                form.Owner = this;
                form.ShowDialog();
            }
           
        }

        private void PostaviSlikuNaPB(PictureBox pb, int vrsta, int kolona)
        {
            pb.Image = matrica.PrikaziSliku(vrsta, kolona);
            pb.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        private void KrajIgre()
        {
            timer.Stop();
            DialogResult dr = MessageBox.Show("Cestitke! Otvorili ste sva polja! Vase vreme: " + tbVreme.Text,
                "Kraj igre", MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        private void ucitajKonfiguracijuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UcitajIzFajl();
            ++klikNaPodesi;
            NapraviNovuIgru();
        }

        private void timer_Tick(object sender, EventArgs e)
        {

            string prikaz = String.Empty;
            vreme++;
            int s, m, sek;
            sek = vreme % 60;
            m = vreme / 60 % 60;
            s = vreme / 3600;
            if (s != 0)
            {
                prikaz = s.ToString() + ":";
            }
            if (m != 0)
            {
                string lala = String.Empty;
                if (m < 10)
                {
                    lala = "0" + m.ToString() + ":";
                }
                else
                {
                    lala = m.ToString() + ":";
                }
                prikaz = prikaz + lala;
            }
            else
            {
                if (s != 0)
                {
                    prikaz += "00:";
                }
            }
            string pom = String.Empty;
            if (sek < 10)
            {
                pom = "0" + sek.ToString();
            }
            else
            {
                pom = sek.ToString();
            }
            prikaz = prikaz + pom;
            tbVreme.Text = prikaz;
        }

        private void MemoryGame_Load(object sender, EventArgs e)
        {

        }

        private void TmVremeNajbrzegKlika_Tick(object sender, EventArgs e)
        {
            string prikaz1 = String.Empty;
            VremeKlika++;
            int s, m, sek;
            sek = VremeKlika % 60;
            m = VremeKlika / 60 % 60;
            s = VremeKlika / 3600;
            if (s != 0)
            {
                prikaz1 = s.ToString() + ":";
            }
            if (m != 0)
            {
                string lala = String.Empty;
                if (m < 10)
                {
                    lala = "0" + m.ToString() + ":";
                }
                else
                {
                    lala = m.ToString() + ":";
                }
                prikaz1 = prikaz1 + lala;
            }
            else
            {
                if (s != 0)
                {
                    prikaz1 += "00:";
                }
            }
            string pom = String.Empty;
            if (sek < 10)
            {
                pom = "0" + sek.ToString();
            }
            else
            {
                pom = sek.ToString();
            }
            prikaz1 = prikaz1 + pom;
            if (BrojKlikaNaSlike == 2) //da su dve slike kliknute
            {
                nizVremena.Add(prikaz1);
                VremeKlika = 0;
            }
           
           // tbVreme.Text = prikaz1;
        }
       //public void PrikaziNajduziINajbrziKlik()
       // {
       //     DateTime minBrKlika = new DateTime();
       //     DateTime maxBrKlika = new DateTime();
       //     //minBrKlika.AddHours(0);
       //     //minBrKlika.AddMinutes(0);
       //     //minBrKlika.AddSeconds(0);
       //     //maxBrKlika.AddHours(12);
       //     //maxBrKlika.AddMinutes(60);
       //     //maxBrKlika.AddSeconds(60);
       //     string[] pomocni1 = new string[3];
       //     pomocni1 = nizVremena[0].Split(':');
       //     minBrKlika.AddHours(int.Parse(pomocni1[0]));
       //     minBrKlika.AddMinutes(int.Parse(pomocni1[1]));
       //     minBrKlika.AddSeconds(int.Parse(pomocni1[2]));
       //     maxBrKlika.AddHours(int.Parse(pomocni1[0]));
       //     maxBrKlika.AddMinutes(int.Parse(pomocni1[1]));
       //     maxBrKlika.AddSeconds(int.Parse(pomocni1[2]));
       //     for (int i=1; i<nizVremena.Count; i++)
       //     {
               
       //         string[] pomocni = new string[3];
       //         pomocni = nizVremena[0].Split(':');
       //         if ( minBrKlika.Minute < int.Parse(pomocni[1]) || minBrKlika.Second < int.Parse(pomocni[2]) )
       //         {
       //             minBrKlika.AddHours(int.Parse(pomocni[0]));
       //             minBrKlika.AddMinutes(int.Parse(pomocni[1]));
       //             minBrKlika.AddSeconds(int.Parse(pomocni[2]));
       //         }
       //     }
       //     for (int i = 1; i < nizVremena.Count; i++)
       //     {

       //         string[] pomocni = new string[3];
       //         pomocni = nizVremena[0].Split(':');
       //         if (maxBrKlika.Minute > int.Parse(pomocni[1]) || maxBrKlika.Second > int.Parse(pomocni[2]) )
       //         {
       //             maxBrKlika.AddHours(int.Parse(pomocni[0]));
       //             maxBrKlika.AddMinutes(int.Parse(pomocni[1]));
       //             maxBrKlika.AddSeconds(int.Parse(pomocni[2]));
       //         }
       //     }
       //     double maxKlika = ((double.Parse(maxBrKlika.Minute.ToString()) / 60%60) + (double.Parse(maxBrKlika.Second.ToString())))/ 1000;
       //     double minKlika = ((double.Parse(minBrKlika.Minute.ToString()) / 60 % 60) + (double.Parse(minBrKlika.Second.ToString()))) / 1000;
       //     // tb1NajbrziKlik.Text = maxBrKlika.Hour.ToString()+":" + maxBrKlika.Minute.ToString() + ":" + max
       //     tb1NajbrziKlik.Text = maxKlika.ToString();
       //     tb2NajsporijiKlik.Text = minKlika.ToString();

       // }
        //private void TmVremeNajsporijegKlika_Tick(object sender, EventArgs e)
        //{
        //    //string prikaz = String.Empty;
        //    //vreme++;
        //    //int s, m, sek;
        //    //sek = vreme % 60;
        //    //m = vreme / 60 % 60;
        //    //s = vreme / 3600;
        //    //if (s != 0)
        //    //{
        //    //    prikaz = s.ToString() + ":";
        //    //}
        //    //if (m != 0)
        //    //{
        //    //    string lala = String.Empty;
        //    //    if (m < 10)
        //    //    {
        //    //        lala = "0" + m.ToString() + ":";
        //    //    }
        //    //    else
        //    //    {
        //    //        lala = m.ToString() + ":";
        //    //    }
        //    //    prikaz = prikaz + lala;
        //    //}
        //    //else
        //    //{
        //    //    if (s != 0)
        //    //    {
        //    //        prikaz += "00:";
        //    //    }
        //    //}
        //    //string pom = String.Empty;
        //    //if (sek < 10)
        //    //{
        //    //    pom = "0" + sek.ToString();
        //    //}
        //    //else
        //    //{
        //    //    pom = sek.ToString();
        //    //}
        //    //prikaz = prikaz + pom;
        //    //tbVreme.Text = prikaz;
        //}

        private void MemoryGame_FormClosing(object sender, FormClosingEventArgs e)
        {

            DialogResult dr = MessageBox.Show("Da li zelite sigurno da napustite? ", "Pitanje", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                DialogResult dr1 = MessageBox.Show("Da li zelite da sacuvate stanje igre? ", "Pitanje", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr1 == DialogResult.Yes)
                {
                    UpisiUFajl(x, y, brSlika, brParova);
                    ++sacuvanFajl;

                }
                klikNaPodesi = 0;
                this.Dispose();
            }
        }



        private void IzbrisiPicBoxove()
        {
            PictureBox pb = new PictureBox();
            this.Controls.OfType<PictureBox>().ToList<PictureBox>().ForEach(pc => pc.Dispose());
        }

        public void UpisiUFajl(int vrs, int kol, int brSl, int brPAr)
        {
            XmlTextWriter wr = null;
            try
            {
                wr = new XmlTextWriter(imeFajla, Encoding.Unicode);
                wr.Formatting = Formatting.Indented;
                wr.WriteStartDocument();
                wr.WriteComment("Primer");
           
                wr.WriteStartElement("root");
                wr.WriteElementString("Vrste", vrs.ToString());

                wr.WriteElementString("Kolone", kol.ToString());

                wr.WriteElementString("Rslike", brSl.ToString());

                wr.WriteElementString("Bparova", brPAr.ToString());

            }
            catch (Exception e)
            {
                MessageBox.Show("Doslo do izuzetka. " + e);
            }
            finally
            {

                wr.WriteEndElement();
                wr.WriteEndDocument();
                wr.Flush();
                wr.Close();
            }
        }
        public void UcitajIzFajl()
        {
            XmlTextReader xmlR = new XmlTextReader(imeFajla);
            try
            {
                //XmlReader reader = XmlReader.Create(imeFajla);

                //string s1 = xmlR.ReadElementContentAsString("Vrsta", "");
                //while (reader.Read())
                //{
                //    if (reader.NodeType == XmlNodeType.Element && reader.Name == "Vrste" && reader.HasValue)
                //    {
                //        //if (reader.HasAttributes)
                //        //{
                //        //    string s = reader.GetAttribute("Vrsta");
                //        //    X = int.Parse(s);
                //        //}
                //        string s = reader.ReadElementContentAsString();
                //    }
                //    if (reader.NodeType == XmlNodeType.Element && reader.Name == "Kolone")
                //    {
                //        string s = reader.ReadContentAsString();
                //        Y = int.Parse(s);
                //    }
                //    if (reader.NodeType == XmlNodeType.Element && reader.Name == "Broj razlicitih slika")
                //    {
                //        string s = reader.ReadContentAsString();
                //        BrSlika = int.Parse(s);
                //    }
                //    if (reader.NodeType == XmlNodeType.Element && reader.Name == "Broj parova")
                //    {
                //        string s = reader.ReadContentAsString();
                //        BrParova = int.Parse(s);
                //    }

                while (xmlR.Read())
                {

                    if (xmlR.Name == "Vrste")
                    {
                        string s = xmlR.ReadString();


                        X = int.Parse(s);
                    }

                    if (xmlR.Name == "Kolone")
                    {
                        string s = xmlR.ReadString();
                        Y = int.Parse(s);
                    }

                    if (xmlR.Name == "Rslike")
                    {
                        string s = xmlR.ReadString();
                        brSlika = int.Parse(s);
                    }

                    if (xmlR.Name == "Bparova")
                    {
                        string s = xmlR.ReadString();
                        brParova = int.Parse(s);
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Doslo je do greske prilikom ucitavanja iz fajla." + e);
            }
            finally
            {
                xmlR.Close();
            }

        }


    }
}
