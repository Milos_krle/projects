﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MemoriIgrica
{
    public partial class PodesavanjeKonfiguracije : Form
    {
        public PodesavanjeKonfiguracije()
        {
            InitializeComponent();
        }
        private void MyInitialization()
        {
            numBrSlika.Minimum = ((MemoryGame)Owner).MinimalanBrSlika;
            numBrSlika.Maximum = ((MemoryGame)Owner).MaksimalanBrSlika;
            numBrParova.Minimum = ((MemoryGame)Owner).MinimalanBrParova;
            numBrParova.Maximum = ((MemoryGame)Owner).MaksimalanBrParova;
            numBrVrsta.Minimum = ((MemoryGame)Owner).MinX;
            numBrVrsta.Maximum = ((MemoryGame)Owner).MaxX;
            numBrKolona.Minimum = ((MemoryGame)Owner).MinY;
            numBrKolona.Maximum = ((MemoryGame)Owner).MaxY;
        }
        public PodesavanjeKonfiguracije(int vrs, int kol, int sl, int par)
        {
            InitializeComponent();
            numBrVrsta.Value =(decimal) vrs;
            numBrKolona.Value = (decimal)kol;
            numBrSlika.Value = (decimal)sl;
            numBrParova.Value = (decimal)par;
        }

        private void PodesavanjeKonfiguracije_Load(object sender, EventArgs e)
        {
            MyInitialization();
        }
        private bool IspravnaVelicinaPolja()
        {
            if (numBrVrsta.Value * numBrKolona.Value < ((MemoryGame)Owner).MinimalnaVelicinaPolja)
            {
               MessageBox.Show("Velicina polja je previse mala. Minimalna velicina polja je 60!");
                return false;
            }
            return true;
        }
        private bool OdnosSlikeParoviIspravan()
        {
            if (numBrParova.Value / 2 < numBrSlika.Value)
            {
                MessageBox.Show("Odnos broja slika i broja parova nije odgovarajuci!");
                return false;
            }
            return true;
        }
        private bool OdnosPolja()
        {
            if (numBrParova.Value * numBrSlika.Value > numBrKolona.Value * numBrVrsta.Value)
            {
                MessageBox.Show("Zahtevani broj polja ne moze da stane u dimenzije koje ste odabrali!");
                return false;
            }
            return true;
        }
      

     
        private void btnExport_Click_1(object sender, EventArgs e)
        {
            bool korektno = true;
            if (!IspravnaVelicinaPolja())
            {
                korektno = false;
            }
            if (!OdnosPolja())
            {
                korektno = false;
            }
           
            if (korektno)
            {
                string vrst = numBrVrsta.Value.ToString();
                string kol = numBrKolona.Value.ToString();
                string slike = numBrSlika.Value.ToString();
                string par = numBrParova.Value.ToString();
                ((MemoryGame)Owner).UpisiUFajl(int.Parse(vrst), int.Parse(kol), int.Parse(slike), int.Parse(par));
                DialogResult dr = MessageBox.Show("Konfiguracija zapamcena", "Obavestenje",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Dispose();
            }
        }

        private void btnPostaviKonfiguraciju_Click_1(object sender, EventArgs e)
        {
            bool korektno = true;
            if (!IspravnaVelicinaPolja())
            {
                korektno = false;
            }
            if (!OdnosPolja())
            {
                korektno = false;
            }

            if (korektno)
            {
                ((MemoryGame)Owner).X = (int)numBrVrsta.Value;
                ((MemoryGame)Owner).Y = (int)numBrKolona.Value;
                ((MemoryGame)Owner).BrSlika = (int)numBrSlika.Value;
                ((MemoryGame)Owner).BrParova = (int)numBrParova.Value;
                ((MemoryGame)Owner).NapraviNovuIgru();
                DialogResult dr = MessageBox.Show("Konfiguracija postavljena i da li zelite da je sacuvate?", "Obavestenje",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    ((MemoryGame)Owner).UpisiUFajl((int)numBrVrsta.Value, (int)numBrKolona.Value, (int)numBrSlika.Value, (int)numBrParova.Value);
                }
                this.Dispose();
            }
        }

    
    }
}
 
