﻿namespace MemoriIgrica
{
    partial class MemoryGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbVreme = new System.Windows.Forms.TextBox();
            this.lblVreme = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.opcijeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novaIgraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prikaziPoljaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.odaberiKonfiguracijuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ucitajKonfiguracijuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izadjiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.TmVremeNajbrzegKlika = new System.Windows.Forms.Timer(this.components);
            this.TmVremeNajsporijegKlika = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbVreme
            // 
            this.tbVreme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbVreme.Enabled = false;
            this.tbVreme.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbVreme.Location = new System.Drawing.Point(724, 12);
            this.tbVreme.Name = "tbVreme";
            this.tbVreme.ReadOnly = true;
            this.tbVreme.Size = new System.Drawing.Size(64, 23);
            this.tbVreme.TabIndex = 6;
            this.tbVreme.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblVreme
            // 
            this.lblVreme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVreme.AutoSize = true;
            this.lblVreme.BackColor = System.Drawing.Color.Lime;
            this.lblVreme.Font = new System.Drawing.Font("Arial Rounded MT Bold", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVreme.Location = new System.Drawing.Point(589, 15);
            this.lblVreme.Name = "lblVreme";
            this.lblVreme.Size = new System.Drawing.Size(114, 16);
            this.lblVreme.TabIndex = 5;
            this.lblVreme.Text = "Proteklo vreme:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.Lime;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opcijeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 38);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "msMeni";
            // 
            // opcijeToolStripMenuItem
            // 
            this.opcijeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novaIgraToolStripMenuItem,
            this.prikaziPoljaToolStripMenuItem,
            this.odaberiKonfiguracijuToolStripMenuItem,
            this.ucitajKonfiguracijuToolStripMenuItem,
            this.izadjiToolStripMenuItem});
            this.opcijeToolStripMenuItem.Name = "opcijeToolStripMenuItem";
            this.opcijeToolStripMenuItem.Size = new System.Drawing.Size(71, 34);
            this.opcijeToolStripMenuItem.Text = "Opcije";
            // 
            // novaIgraToolStripMenuItem
            // 
            this.novaIgraToolStripMenuItem.Name = "novaIgraToolStripMenuItem";
            this.novaIgraToolStripMenuItem.Size = new System.Drawing.Size(248, 26);
            this.novaIgraToolStripMenuItem.Text = "Nova igra";
            this.novaIgraToolStripMenuItem.Click += new System.EventHandler(this.novaIgraToolStripMenuItem_Click);
            // 
            // prikaziPoljaToolStripMenuItem
            // 
            this.prikaziPoljaToolStripMenuItem.Name = "prikaziPoljaToolStripMenuItem";
            this.prikaziPoljaToolStripMenuItem.Size = new System.Drawing.Size(248, 26);
            this.prikaziPoljaToolStripMenuItem.Text = "Prikazi polja";
            this.prikaziPoljaToolStripMenuItem.Click += new System.EventHandler(this.prikaziPoljaToolStripMenuItem_Click);
            // 
            // odaberiKonfiguracijuToolStripMenuItem
            // 
            this.odaberiKonfiguracijuToolStripMenuItem.Name = "odaberiKonfiguracijuToolStripMenuItem";
            this.odaberiKonfiguracijuToolStripMenuItem.Size = new System.Drawing.Size(248, 26);
            this.odaberiKonfiguracijuToolStripMenuItem.Text = "Odaberi konfiguraciju";
            this.odaberiKonfiguracijuToolStripMenuItem.Click += new System.EventHandler(this.odaberiKonfiguracijuToolStripMenuItem_Click);
            // 
            // ucitajKonfiguracijuToolStripMenuItem
            // 
            this.ucitajKonfiguracijuToolStripMenuItem.Name = "ucitajKonfiguracijuToolStripMenuItem";
            this.ucitajKonfiguracijuToolStripMenuItem.Size = new System.Drawing.Size(248, 26);
            this.ucitajKonfiguracijuToolStripMenuItem.Text = "Ucitaj konfiguraciju";
            this.ucitajKonfiguracijuToolStripMenuItem.Click += new System.EventHandler(this.ucitajKonfiguracijuToolStripMenuItem_Click);
            // 
            // izadjiToolStripMenuItem
            // 
            this.izadjiToolStripMenuItem.Name = "izadjiToolStripMenuItem";
            this.izadjiToolStripMenuItem.Size = new System.Drawing.Size(248, 26);
            this.izadjiToolStripMenuItem.Text = "Izadji";
            this.izadjiToolStripMenuItem.Click += new System.EventHandler(this.izadjiToolStripMenuItem_Click);
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // TmVremeNajbrzegKlika
            // 
            this.TmVremeNajbrzegKlika.Interval = 1000;
            this.TmVremeNajbrzegKlika.Tick += new System.EventHandler(this.TmVremeNajbrzegKlika_Tick);
            // 
            // TmVremeNajsporijegKlika
            // 

            // 
            // MemoryGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MemoriIgrica.Properties.Resources.prikaz;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tbVreme);
            this.Controls.Add(this.lblVreme);
            this.Controls.Add(this.menuStrip1);
            this.Name = "MemoryGame";
            this.Text = "Igrica memorije";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MemoryGame_FormClosing);
            this.Load += new System.EventHandler(this.MemoryGame_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tbVreme;
        private System.Windows.Forms.Label lblVreme;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem opcijeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novaIgraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prikaziPoljaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem odaberiKonfiguracijuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ucitajKonfiguracijuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izadjiToolStripMenuItem;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Timer TmVremeNajbrzegKlika;
        private System.Windows.Forms.Timer TmVremeNajsporijegKlika;
    }
}

