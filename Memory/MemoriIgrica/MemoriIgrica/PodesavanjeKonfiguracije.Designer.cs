﻿namespace MemoriIgrica
{
    partial class PodesavanjeKonfiguracije
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PodesavanjeKonfiguracije));
            this.btnPostaviKonfiguraciju = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.pbSlika = new System.Windows.Forms.PictureBox();
            this.numBrKolona = new System.Windows.Forms.NumericUpDown();
            this.lblBrKolona = new System.Windows.Forms.Label();
            this.numBrVrsta = new System.Windows.Forms.NumericUpDown();
            this.lblBrVrsta = new System.Windows.Forms.Label();
            this.numBrParova = new System.Windows.Forms.NumericUpDown();
            this.lblBrParova = new System.Windows.Forms.Label();
            this.lblBrSlika = new System.Windows.Forms.Label();
            this.numBrSlika = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.pbSlika)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBrKolona)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBrVrsta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBrParova)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBrSlika)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPostaviKonfiguraciju
            // 
            this.btnPostaviKonfiguraciju.BackColor = System.Drawing.Color.Chartreuse;
            this.btnPostaviKonfiguraciju.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPostaviKonfiguraciju.Location = new System.Drawing.Point(230, 160);
            this.btnPostaviKonfiguraciju.Margin = new System.Windows.Forms.Padding(2);
            this.btnPostaviKonfiguraciju.Name = "btnPostaviKonfiguraciju";
            this.btnPostaviKonfiguraciju.Size = new System.Drawing.Size(130, 48);
            this.btnPostaviKonfiguraciju.TabIndex = 22;
            this.btnPostaviKonfiguraciju.Text = "Postavi konfiguraciju";
            this.btnPostaviKonfiguraciju.UseVisualStyleBackColor = false;
            this.btnPostaviKonfiguraciju.Click += new System.EventHandler(this.btnPostaviKonfiguraciju_Click_1);
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.Orange;
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnExport.Location = new System.Drawing.Point(230, 221);
            this.btnExport.Margin = new System.Windows.Forms.Padding(2);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(130, 46);
            this.btnExport.TabIndex = 21;
            this.btnExport.Text = "Zapamti konfiguraciju";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click_1);
            // 
            // pbSlika
            // 
            this.pbSlika.Image = ((System.Drawing.Image)(resources.GetObject("pbSlika.Image")));
            this.pbSlika.Location = new System.Drawing.Point(19, 143);
            this.pbSlika.Margin = new System.Windows.Forms.Padding(2);
            this.pbSlika.Name = "pbSlika";
            this.pbSlika.Size = new System.Drawing.Size(164, 157);
            this.pbSlika.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSlika.TabIndex = 20;
            this.pbSlika.TabStop = false;
            // 
            // numBrKolona
            // 
            this.numBrKolona.Location = new System.Drawing.Point(320, 84);
            this.numBrKolona.Margin = new System.Windows.Forms.Padding(2);
            this.numBrKolona.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numBrKolona.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numBrKolona.Name = "numBrKolona";
            this.numBrKolona.Size = new System.Drawing.Size(62, 20);
            this.numBrKolona.TabIndex = 19;
            this.numBrKolona.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numBrKolona.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // lblBrKolona
            // 
            this.lblBrKolona.AutoSize = true;
            this.lblBrKolona.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrKolona.Location = new System.Drawing.Point(216, 84);
            this.lblBrKolona.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBrKolona.Name = "lblBrKolona";
            this.lblBrKolona.Size = new System.Drawing.Size(95, 17);
            this.lblBrKolona.TabIndex = 18;
            this.lblBrKolona.Text = "Broj kolona:";
            // 
            // numBrVrsta
            // 
            this.numBrVrsta.Location = new System.Drawing.Point(121, 87);
            this.numBrVrsta.Margin = new System.Windows.Forms.Padding(2);
            this.numBrVrsta.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numBrVrsta.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numBrVrsta.Name = "numBrVrsta";
            this.numBrVrsta.Size = new System.Drawing.Size(62, 20);
            this.numBrVrsta.TabIndex = 17;
            this.numBrVrsta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numBrVrsta.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            // 
            // lblBrVrsta
            // 
            this.lblBrVrsta.AutoSize = true;
            this.lblBrVrsta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrVrsta.Location = new System.Drawing.Point(28, 86);
            this.lblBrVrsta.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBrVrsta.Name = "lblBrVrsta";
            this.lblBrVrsta.Size = new System.Drawing.Size(88, 17);
            this.lblBrVrsta.TabIndex = 16;
            this.lblBrVrsta.Text = "Broj vrsta: ";
            // 
            // numBrParova
            // 
            this.numBrParova.Location = new System.Drawing.Point(320, 38);
            this.numBrParova.Margin = new System.Windows.Forms.Padding(2);
            this.numBrParova.Name = "numBrParova";
            this.numBrParova.Size = new System.Drawing.Size(62, 20);
            this.numBrParova.TabIndex = 15;
            this.numBrParova.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numBrParova.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // lblBrParova
            // 
            this.lblBrParova.AutoSize = true;
            this.lblBrParova.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrParova.Location = new System.Drawing.Point(216, 37);
            this.lblBrParova.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBrParova.Name = "lblBrParova";
            this.lblBrParova.Size = new System.Drawing.Size(97, 17);
            this.lblBrParova.TabIndex = 14;
            this.lblBrParova.Text = "Broj parova:";
            // 
            // lblBrSlika
            // 
            this.lblBrSlika.AutoSize = true;
            this.lblBrSlika.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrSlika.Location = new System.Drawing.Point(28, 38);
            this.lblBrSlika.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBrSlika.Name = "lblBrSlika";
            this.lblBrSlika.Size = new System.Drawing.Size(80, 17);
            this.lblBrSlika.TabIndex = 13;
            this.lblBrSlika.Text = "Broj slika:";
            // 
            // numBrSlika
            // 
            this.numBrSlika.Location = new System.Drawing.Point(121, 36);
            this.numBrSlika.Margin = new System.Windows.Forms.Padding(2);
            this.numBrSlika.Name = "numBrSlika";
            this.numBrSlika.Size = new System.Drawing.Size(62, 20);
            this.numBrSlika.TabIndex = 12;
            this.numBrSlika.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numBrSlika.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // PodesavanjeKonfiguracije
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(400, 336);
            this.Controls.Add(this.btnPostaviKonfiguraciju);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.pbSlika);
            this.Controls.Add(this.numBrKolona);
            this.Controls.Add(this.lblBrKolona);
            this.Controls.Add(this.numBrVrsta);
            this.Controls.Add(this.lblBrVrsta);
            this.Controls.Add(this.numBrParova);
            this.Controls.Add(this.lblBrParova);
            this.Controls.Add(this.lblBrSlika);
            this.Controls.Add(this.numBrSlika);
            this.MaximizeBox = false;
            this.Name = "PodesavanjeKonfiguracije";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PodesavanjeKonfiguracije";
            ((System.ComponentModel.ISupportInitialize)(this.pbSlika)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBrKolona)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBrVrsta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBrParova)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBrSlika)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPostaviKonfiguraciju;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.PictureBox pbSlika;
        private System.Windows.Forms.NumericUpDown numBrKolona;
        private System.Windows.Forms.Label lblBrKolona;
        private System.Windows.Forms.NumericUpDown numBrVrsta;
        private System.Windows.Forms.Label lblBrVrsta;
        private System.Windows.Forms.NumericUpDown numBrParova;
        private System.Windows.Forms.Label lblBrParova;
        private System.Windows.Forms.Label lblBrSlika;
        private System.Windows.Forms.NumericUpDown numBrSlika;
    }
}