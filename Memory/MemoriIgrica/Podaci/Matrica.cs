﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Podaci
{
   public class Matrica
    {
        private Podaci[,] matrica;
        private int brParova, brVrsta, brKolona, brSlika;
        private int poslednjeX, poslednjeY, trenutniX, trenutniY;
        private int brOtvorenihSlika;
        public Matrica(int n, int m, int vrste, int kolone)
        {
            poslednjeX = poslednjeY = trenutniX = trenutniY = -1;
            brSlika = m;
            brParova = n;
            brVrsta = vrste;
            brKolona = kolone;
            brOtvorenihSlika = 0;
            matrica = new Podaci[brVrsta, brKolona];
            for (int i = 0; i < brVrsta; i++)
            {
                for (int j = 0; j < brKolona; j++)
                {
                    matrica[i, j] = new Podaci();
                }
            }
            RasporediSlike();
        }
        public int PoslednjeX
        {
            get { return poslednjeX; }
        }
        public int PoslednjeY
        {
            get { return poslednjeY; }
        }
        public Image Pozadina
        {
            get { return matrica[0, 0].Pozadina; }
        }
        public void OtvoriSveSlike()
        {
            for (int i = 0; i < brVrsta; i++)
            {
                for (int j = 0; j < brKolona; j++)
                {
                    matrica[i, j].Stats = (Status)2;
                }
            }
        }
        public void ResetujPoslednjeOtvorene()
        {
            poslednjeX = -1;
            poslednjeY = -1;
        }
        public void ResetujTrenutnoOtvorene()
        {
            trenutniX = -1;
            trenutniY = -1;
        }
        public void OtvoriSliku(int i, int j)
        {
            matrica[i, j].Stats = (Status)2;
        }
        public Image OtvoriPoslednjuOtvorenu()
        {
            matrica[poslednjeX, poslednjeY].Stats = (Status)2;
            return matrica[poslednjeX, poslednjeY].SlikaImage;
        }
        public Image OtvoriTrenutnoOtvorenu()
        {
            matrica[trenutniX, trenutniY].Stats = (Status)2;
            return matrica[trenutniX, trenutniY].SlikaImage;
        }
        public Image PrivremenoOtvoriSliku(int i, int j)
        {
            matrica[i, j].Stats = (Status)1;
            trenutniX = i;
            trenutniY = j;
            brOtvorenihSlika++;
            return PrikaziSliku(i, j);
        }
        public Image PrikaziSliku(int i, int j)
        {
            if ((int)matrica[i, j].Stats == 0)
                return matrica[i, j].Pozadina;
            else
                return matrica[i, j].SlikaImage;
        }
        public Image ZatvoriSliku(int i, int j)
        {
            matrica[i, j].Stats = Status.Zatvorena;
            brOtvorenihSlika--;
            return matrica[i, j].Pozadina;
        }
        private void RasporediSlike()
        {
            Random rnd = new Random();
            for (int i = 0; i < brSlika; i++)
            {
                for (int j = 0; j < brParova; j++)
                {
                    int x, y;
                    do
                    {
                        x = rnd.Next(brVrsta);
                        y = rnd.Next(brKolona);
                    }
                    while (matrica[x, y].Slika != matrica[x, y].PraznaSlikaNum);
                    matrica[x, y].Slika = i;
                }
            }
        }
        private bool DaLiJeIstaLokacija()
        {
            if (trenutniX == poslednjeX && trenutniY == poslednjeY)
                return true;
            return false;
        }
        private bool DaLiSuPrethodniNeinicijalizovani()
        {
            if (poslednjeX == -1 || poslednjeY == -1)
                return true;
            return false;
        }
        public bool DaLiSuIste()
        {
            if (!DaLiSuPrethodniNeinicijalizovani() && !DaLiJeIstaLokacija())
            {
                if (matrica[trenutniX, trenutniY].Slika == matrica[poslednjeX, poslednjeY].Slika)
                    return true;
            }
            return false;
        }
        public bool DaLiJeZatvorena(int i, int j)
        {
            if (matrica[i, j].Stats == (Status)0)
            {
                return true;
            }
            return false;
        }
        public Image ZatvoriPrethodnuSliku()
        {
            matrica[poslednjeX, poslednjeY].Stats = (Status)0;
            brOtvorenihSlika--;
            return PrikaziSliku(poslednjeX, poslednjeY);
        }
        public Image ZatvoriTrenutnoOtvorenuSLiku()
        {
            matrica[trenutniX, trenutniY].Stats = (Status)0;
            brOtvorenihSlika--;
            return PrikaziSliku(trenutniX, trenutniY);
        }
        public void PromeniStanjeTrenutniPoslednji()
        {
            poslednjeX = trenutniX;
            poslednjeY = trenutniY;
        }
        public bool DaLiJePrazna(int i, int j)
        {
            if (matrica[i, j].Slika == matrica[i, j].PraznaSlikaNum)
                return true;
            return false;
        }
        public bool DaLiJeKrajIgre()
        {
            if (brOtvorenihSlika == brVrsta * brKolona)
                return true;
            return false;
        }
        public Image VratiSliku(int i, int j)
        {
            return matrica[i, j].SlikaImage;
        }
    }
}
