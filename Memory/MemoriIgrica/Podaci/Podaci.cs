﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Podaci
{
   
        public enum Status
        {
            Zatvorena,
            Privremeno_otvorena,
            Otvorena
        }
    public class Podaci
    {
        private static Slike nizSlika;
        private Status status;
        private int slika;
        static Podaci()
        {
            nizSlika = new Slike();
        }
        public Podaci()
        {
            status = (Status)0;
            slika = 13;
        }
        private Podaci(int s, Status stat)
        {
            slika = s;
            status = stat;
        }
        public Status Stats
        {
            get { return status; }
            set { status = value; }
        }
        public int Slika
        {
            get { return slika; }
            set { slika = value; }
        }
        public Image SlikaImage
        {
            get { return nizSlika[slika]; }
        }
        public Image Pozadina
        {
            get { return nizSlika.Pozadina; }
        }
        public int PozadinaNum
        {
            get { return nizSlika.PozadinaNum; }
        }
        public Image PraznaSlika
        {
            get { return nizSlika.PraznaSlika; }
        }
        public int PraznaSlikaNum
        {
            get { return nizSlika.PraznaSlikaNum; }
        }
    }
}
