﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Reflection;

namespace Podaci
{
   public class Slike
    {
        public Slike()
        {
            slike = new Image[14];
            slike[0] = Properties.Resources._0;
            slike[1] = Properties.Resources._1;
            slike[2] = Properties.Resources._2;
            slike[3] = Properties.Resources._3;
            slike[4] = Properties.Resources._4;
            slike[5] = Properties.Resources._5;
            slike[6] = Properties.Resources._6;
            slike[7] = Properties.Resources._7;
            slike[8] = Properties.Resources._8;
            slike[9] = Properties.Resources._9;
            slike[10] = Properties.Resources._10;
            slike[11] = Properties.Resources._11;
            slike[12] = Properties.Resources._12;
            slike[13] = Properties.Resources._13;
        }
        private Image[] slike;
        public Image this[int i]
        {
            get { return slike[i]; }
        }
        public Image Pozadina
        {
            get { return slike[12]; }
        }
        public int PozadinaNum
        {
            get { return 12; }
        }
        public Image PraznaSlika
        {
            get { return slike[13]; }
        }
        public int PraznaSlikaNum
        {
            get { return 13; }
        }
    }
}
